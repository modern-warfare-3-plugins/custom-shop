﻿using Addon;
using System.Collections.Generic;
using System.Linq;

namespace CustomShop
{
    public class TeamCounter : CPlugin
    {
        public List<ServerClient> Allies()
        {
            return PlayersForTeam(Teams.Allies);
        }

        public List<ServerClient> Axis()
        {
            return PlayersForTeam(Teams.Axis);
        }

        public int TeamCount(Team team)
        {
            var allies = 0;
            var axis = 0;

            foreach (var client in GetClients())
            {
                switch (client.Team)
                {
                    case Teams.Allies:
                        allies++;
                        break;

                    case Teams.Axis:
                        axis++;
                        break;
                }
            }

            return team == Team.Allies ? allies : axis;
        }

        public ServerClient GetLastAlive()
        {
            return TeamCount(Team.Allies) == 1 ? GetClients().FirstOrDefault(client => client.Team == Teams.Allies) : null;
        }

        private List<ServerClient> PlayersForTeam(Teams team)
        {
            var list = new List<ServerClient>();
            list.AddRange(GetClients().Where(client => client.Team == team));

            return list;
        }
    }

    public enum Team
    {
        Allies = 1,
        Axis = 2
    }
}