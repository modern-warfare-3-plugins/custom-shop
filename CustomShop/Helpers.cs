﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;

namespace CustomShop
{
    internal static class Helpers
    {
        /// <summary>
        /// Converts the given item names enum value from a string to an integer
        /// </summary>
        /// <param name="itemName"></param>
        /// <returns></returns>
        public static int ItemPrice(string itemName)
        {
            return (int) Enum.Parse(typeof (Prices), itemName);
        }

        /// <summary>
        /// Formats the total number of points given if the value is greater or equal to 1000
        /// </summary>
        /// <param name="total"></param>
        /// <returns></returns>
        public static string FormatPoints(int total)
        {
            return total >= 1000 ? string.Format("{0:#,#}", total) : total.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Finds and returns the enum description for the given enum name
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetEnumDescription(Enum value)
        {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[]) fi.GetCustomAttributes(typeof (DescriptionAttribute), false);

            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }

        /// <summary>
        /// Returns a list of enums that are contained with the enum type given
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> EnumToList<T>()
        {
            var enumType = typeof (T);

            if (enumType.BaseType != typeof (Enum))
            {
                return null;
            }

            var enumValArray = Enum.GetValues(enumType);
            var enumValList = new List<T>(enumValArray.Length);
            
            enumValList.AddRange(from int val in enumValArray
                                 select (T) Enum.Parse(enumType, val.ToString(CultureInfo.InvariantCulture)));

            return enumValList;
        }

        /// <summary>
        /// Replaces a single quote with an additional single quote which escapes it preventing SQL errors
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string EscapeSqlQuotes(string input)
        {
            return input.Replace("'", "''");
        }

        /// <summary>
        /// Unescapes the escaped single quote within the given string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string UnEscapeSqlQuotes(string input)
        {
            return input.Replace("''", "'");
        }
    }
}