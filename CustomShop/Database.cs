﻿using Addon;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;

namespace CustomShop
{
    internal sealed class Database : CPlugin
    {
        private static readonly object      LockThis    = new object();

        private static Database             _instance;
        private static SQLiteConnection     _db;

        /// <summary>
        /// Returns the static instance of the SQLite database class, this is thread safe to prevent
        /// server lock ups and some other tragic events from occurring
        /// </summary>
        public static Database GetInstance
        {
            get
            {
                lock (LockThis)
                {
                    return _instance ?? (_instance = new Database());
                }
            }
        }

        /// <summary>
        /// The constructor for the SQLite database class, this can only be accessed via the instance
        /// created by the singleton
        /// </summary>
        private Database()
        {
            try
            {
                _db = new SQLiteConnection("Data Source=shop.sqlite");
                _db.Open();
            }
            catch (SQLiteException ex)
            {
                ServerError(ErrorType.ErrorCritical, ex.Message, "Database");
            }
        }

        /// <summary>
        /// The destructor for the SQLite database class, this can't be invoked manually
        /// </summary>
        ~Database()
        {
            _db.Close();
        }

        /// <summary>
        /// Creates a new SQLiteCommand using the SQL query given
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        private static SQLiteCommand Command(string sql)
        {
            return new SQLiteCommand(_db) {CommandText = sql};
        }

        /// <summary>
        /// Creates an SQLite friendly "datetime" field value
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public string DateTimeFormat(DateTime datetime)
        {
            return string.Format("{0}-{1}-{2} {3}:{4}:{5}.{6}", datetime.Year, datetime.Month, datetime.Day,
                                 datetime.Hour, datetime.Minute, datetime.Second, datetime.Millisecond);
        }

        /// <summary>
        /// Executes a generic query against a table in the database
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable GetDataTable(string sql)
        {
            var dt = new DataTable();

            try
            {
                var reader = Command(sql).ExecuteReader();

                dt.Load(reader);
                reader.Close();
            }
            catch (SQLiteException ex)
            {
                ServerLog(LogType.LogData, ex.ToString());
            }

            return dt;
        }

        /// <summary>
        /// Executes a query on the database that doesn't result in any data being returned
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sql)
        {
            return Command(sql).ExecuteNonQuery();
        }

        /// <summary>
        /// Executes a query that returns a single item value
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public string ExecuteScalar(string sql)
        {
            var value = Command(sql).ExecuteScalar();
            return value != null ? value.ToString() : "";
        }

        /// <summary>
        /// Constructs an update query that is run against the database to update a range of table
        /// rows, this query requires a where claus
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="data"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public bool Update(string tableName, Dictionary<string, string> data, string where)
        {
            var vals = "";
            var returnCode = true;

            if (data.Count >= 1)
            {
                vals = data.Aggregate(vals, (x, v) => x + string.Format(" {0} = '{1}',", v.Key, v.Value));
                vals = vals.Substring(0, vals.Length - 1);
            }

            var sqlQuery = string.Format("UPDATE {0} SET{1} WHERE {2};", tableName, vals, where);

            try
            {
                ExecuteNonQuery(sqlQuery);
            }
            catch (SQLiteException ex)
            {
                ServerPrint(string.Format("[QUERY] {0}\nUpdate(): {1}", sqlQuery, ex.Message));
                returnCode = false;
            }

            return returnCode;
        }

        /// <summary>
        /// Executes a query that allows either a single row or multiple rows to be deleted from
        /// the SQLite database
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        public bool Delete(string tableName, string where)
        {
            var returnCode = true;

            try
            {
                ExecuteNonQuery(string.Format("DELETE FROM {0} WHERE {1};", tableName, where));
            }
            catch (Exception ex)
            {
                ServerLog(LogType.LogData, ex.ToString());
                returnCode = false;
            }

            return returnCode;
        }

        /// <summary>
        /// Executes an SQL query that inserts a new table record into the database
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool Insert(string tableName, Dictionary<string, string> data)
        {
            var columns = "";
            var values = "";
            var returnCode = true;

            foreach (var val in data)
            {
                columns += string.Format(" {0},", val.Key);
                values += string.Format(" '{0}',", val.Value);
            }

            columns = columns.Substring(0, columns.Length - 1);
            values = values.Substring(0, values.Length - 1);

            try
            {
                ExecuteNonQuery(string.Format("INSERT INTO {0} ({1}) VALUES({2});", tableName, columns, values));
            }
            catch (Exception ex)
            {
                ServerLog(LogType.LogData, ex.ToString());
                returnCode = false;
            }

            return returnCode;
        }

        /// <summary>
        /// Retrieves the specified column value for the given player XUID
        /// </summary>
        /// <param name="field"></param>
        /// <param name="id"></param>
        /// <param name="table"></param>
        /// <param name="playerId"></param>
        /// <returns></returns>
        public T GetPlayerData<T>(string field, string id, string table = "Players", bool playerId = false)
        {
            var type = playerId ? "player_id" : "xuid";
            var query = string.Format("SELECT {0} FROM {1} WHERE {2}='{3}';", field, table, type, id);
            
            try
            {
                return (T) Convert.ChangeType(ExecuteScalar(query), typeof (T));
            }
            catch (SQLiteException ex)
            {
                ServerLog(LogType.LogConsole, ex.Message);
            }

            return default(T);
        }

        /// <summary>
        /// Retrieves data for the specified columns for the given player id
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="playerId"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public DataTable GetPlayerData(string[] fields, int playerId, string table = "ItemsToHumans")
        {
            return
                GetDataTable(string.Format("SELECT {0} FROM {1} WHERE player_id={2};", string.Join(",", fields), table,
                                           playerId));
        }

        /// <summary>
        /// Returns a boolean that allows us to determine if the given player XUID exists in the
        /// database, if it doesn't "false" will be returned
        /// </summary>
        /// <param name="xuid"></param>
        /// <returns></returns>
        public bool PlayerExists(string xuid)
        {
            return GetPlayerData<int>("count(*)", xuid) == 1;
        }

        /// <summary>
        /// Inserts a new player record into the database
        /// </summary>
        /// <param name="data"></param>
        public void CreatePlayerRecord(ClientData data)
        {
            Insert("Players", new Dictionary<string, string>
                {
                    {"name", data.Name},
                    {"xuid", data.Xuid},
                    {"points", data.Points.ToString(CultureInfo.InvariantCulture)},
                    {"last_online", DateTimeFormat(DateTime.Now)}
                });
        }

        /// <summary>
        /// Updates the clients data for the given table
        /// </summary>
        /// <param name="data"></param>
        /// <param name="table"></param>
        public void UpdatePlayerRecord(ClientData data, string table = "Players")
        {
            Update(table, data.TableData, string.Format("xuid='{0}'", data.Xuid));
        }

        /// <summary>
        /// Updates the clients data for the given table
        /// </summary>
        /// <param name="data"></param>
        /// <param name="playerId"></param>
        /// <param name="table"></param>
        public void UpdatePlayerRecord(ClientData data, int playerId, string table = "ItemsToHumans")
        {
            Update(table, data.TableData, string.Format("player_id={0}", playerId));
        }

        /// <summary>
        /// Updates the players total number of points in the database
        /// </summary>
        /// <param name="xuid"></param>
        /// <param name="points"></param>
        public void UpdatePlayerPoints(string xuid, int points)
        {
            UpdatePlayerRecord(new ClientData
                {
                    TableData = new Dictionary<string, string>
                        {
                            {"points", points.ToString(CultureInfo.InvariantCulture)}
                        },
                    Xuid = xuid
                });
        }

        /// <summary>
        /// Updates the clients item data, this data is updated based on the current team they're on
        /// </summary>
        /// <param name="xuid"></param>
        /// <param name="team"></param>
        /// <param name="slot"></param>
        /// <param name="value"></param>
        public void UpdatePlayeritemData(string xuid, Teams team, ItemSlot slot, string value = "")
        {
            var table = team == Teams.Allies ? "ItemsToHumans" : "ItemsToInfected";
            var playerId = GetPlayerData<string>("id", xuid);

            // Does the player have an entry in the table? If not create one first
            if (GetPlayerData<int>("count(*)", playerId, table, true) == 0)
            {
                var data = new Dictionary<string, string>
                    {
                        {"player_id", playerId},
                        {"primary_weapon", ""},
                        {"secondary_weapon", ""}
                    };

                // If the player is on the infected team add the equipment field
                if (team == Teams.Axis)
                {
                    data.Add("equipment", "");
                }

                Insert(table, data);
            }

            // Determine the weapon slot and update it
            string columnName = null;

            switch (slot)
            {
                case ItemSlot.Equipment:
                    columnName = (team == Teams.Axis) ? "equipment" : null;
                    break;

                case ItemSlot.Primary:
                    columnName = "primary_weapon";
                    break;

                case ItemSlot.Secondary:
                    columnName = "secondary_weapon";
                    break;
            }

            if (string.IsNullOrEmpty(columnName)) return;

            // Update the players record
            UpdatePlayerRecord(new ClientData
                {
                    TableData = new Dictionary<string, string>
                        {
                            {columnName, value}
                        }
                }, int.Parse(playerId), table);
        }

        /// <summary>
        /// Removes the given player from the database completely
        /// </summary>
        /// <param name="xuid"></param>
        public void DeletePlayer(string xuid)
        {
            var playerId = GetPlayerData<int>("id", xuid);

            Delete("ItemsToHumans", string.Format("player_id={0}", playerId));
            Delete("ItemsToInfected", string.Format("player_id={0}", playerId));
            Delete("Players", string.Format("xuid='{0}'", xuid));
        }
    }

    /// <summary>
    /// Holds generic information about the client including their name, xuid and points
    /// </summary>
    internal class ClientData
    {
        public string Name, Xuid;
        public int Points = 0;
        public Dictionary<string, string> TableData;
    }
}