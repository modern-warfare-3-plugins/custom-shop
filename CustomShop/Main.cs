﻿using Addon;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

namespace CustomShop
{
    public class Main : TeamCounter
    {
        // Lists and dictionaries
        private readonly List<int>                          _topPoints                  = new List<int>();
        private readonly List<string>                       _topPlayers                 = new List<string>();
        private List<string>                                _commands;

        private Dictionary<string, string>                  _infoData                   = new Dictionary<string, string>();
        private readonly Dictionary<string, List<int>>      _helpHuds                   = new Dictionary<string, List<int>>();
        private readonly Dictionary<string, PlayerData>     _playerData                 = new Dictionary<string, PlayerData>();
        private readonly Dictionary<int, PlayerHuds>        _playerStats                = new Dictionary<int, PlayerHuds>();

        // Misc variables
        private readonly Database                           _db                         = Database.GetInstance;
        private TeamItems                                   _shopItemsHud;
        private ShopItems                                   _shopItems;
        private Thread                                      _pointsThread;
        private DateTime                                    _bansLastUpdate;
        private int                                         _pointsThreadTicker;
        private int                                         _counter                    = -1;
        private int                                         _pointsTicker;
        private bool                                        _doublePoints;

        // Delegates
        private delegate string ItemExec(ShopItem item, ref ServerClient client, string command);
        
        /// <summary>
        /// Executes when the MW3 server loads
        /// </summary>
        public override void OnServerLoad()
        {
            var assembly = Assembly.GetExecutingAssembly().GetName();
            var fileInfo = FileVersionInfo.GetVersionInfo(string.Format(@"plugins\{0}.dll", assembly.Name));

            ServerPrint("\nCustom shop by SgtLegend loaded");
            ServerPrint(string.Format("Version: {0} | Build: {1}\n", fileInfo.FileVersion, assembly.Version));

            // Load all the players from the old points file and convert them into SQLite records
            if (File.Exists("player_points.txt"))
            {
                _pointsThread = new Thread(LoadAndConvertPlayerPoints);
                _pointsThread.Start();
            }

            // Load the shop info data from the HDD
            LoadShopInfoFromDisk();

            // Initial double points and points reset checks
            SetDoublePointsWeekendStatus();
            CheckIfPlayerPointsNeedToBeReset();

            // Create a new instance of the shop items class
            _shopItems = new ShopItems();

            // Setup the commands
            _commands =
                new List<string>(
                    string.Format(
                        "info help points mp point shop weapons smg lmg assault snipe shotguns expl handgun resetweapon resetweapons dp givepoints takepoints spyon sendpoints {0}",
                        _shopItems.GetItemCommands()).Split(' '));

            // Shop items for the help huds
            _shopItemsHud = new TeamItems
                {
                    Allies = _shopItems.GetItemsAsInfoList(new List<Teams> {Teams.Allies}),
                    Axis = _shopItems.GetItemsAsInfoList(new List<Teams> {Teams.Axis})
                };

            // Get the last known write time for the "permanent.ban" file
            _bansLastUpdate = File.GetLastWriteTime(@"main\permanent.ban");

            // Set a timer that runs every 10 seconds that updates the help huds for the humans
            // weapons list
            Timing.AfterDelay(10000, () => Timing.OnInterval(10000, UpdateWeaponHelpHuds));

            // Set a timer that runs once per minute that checks if the shop needs to toggle
            // double points
            Timing.AfterDelay(120000, () => Timing.OnInterval(120000, CheckPointsStatus));
        }

        /// <summary>
        /// Processes every time the server addon calls the MW3 server frame
        /// </summary>
        public override void OnServerFrame()
        {
            Timing.ProcessFrame(GetClients());
        }

        /// <summary>
        /// Executes every 1/4th of a second
        /// </summary>
        public override void OnAddonFrame()
        {
            if (_counter >= 0)
            {
                _counter++;

                if (_counter == (1000/AddonFrameInterval))
                {
                    // Determine the highest scoring players
                    CalculateTopPlayers();

                    // Create the help huds
                    CreateHelpHuds();

                    // Create the high score huds
                    CreateHighScoreHuds();

                    // Set the weapon id's for the shop
                    _shopItems.SetWeaponIds();

                    _counter = -1;
                }
            }

            // If the counter doesn't equal "-1" stop here to hopefully prevent any extra exceptions that
            // may occur below with the player health huds
            if (_counter != -1) return;

            // Health hud updates
            try
            {
                var clients = GetClients();
                if (clients == null || _playerStats.Count == 0) return;

                foreach (var client in clients.Where(x => x.ConnectionState == ConnectionStates.Connected && x.Other.isAlive))
                {
                    if (!_playerStats.ContainsKey(client.ClientNum)) continue;

                    var health = client.Other.Health;
                    var hud = GetHudElement(_playerStats[client.ClientNum].Health);

                    if (hud != null && hud.GetType() == typeof (HudElem))
                    {
                        hud.SetString(string.Format("^:Health: ^{0}{1}",
                                                    (health <= 40 ? "1" : (health >= 41 && health <= 70 ? "3" : "2")),
                                                    health));
                    }
                }
            }
            catch (Exception ex)
            {
                ServerLog(LogType.LogConsole, ex.ToString());
            }
        }

        /// <summary>
        /// Executes when a player connects to the server
        /// </summary>
        /// <param name="client"></param>
        public override void OnPlayerConnect(ServerClient client)
        {
            if (TestClients.IsBot(client)) return;

            // Create a new player record if they don't have one
            if (!_db.PlayerExists(client.XUID))
            {
                _db.CreatePlayerRecord(new ClientData
                    {
                        Name = Helpers.EscapeSqlQuotes(client.Name),
                        Xuid = client.XUID,
                        Points = 0
                    });
            }
            else
            {
                _db.UpdatePlayerRecord(new ClientData
                    {
                        Xuid = client.XUID,
                        TableData = new Dictionary<string, string>
                            {
                                {"name", Helpers.EscapeSqlQuotes(client.Name)},
                                {"last_online", _db.DateTimeFormat(DateTime.Now)}
                            }
                    });
            }

            // Set the players data locally
            if (!_playerData.ContainsKey(client.XUID))
            {
                _playerData.Add(client.XUID, new PlayerData
                    {
                        Name = Helpers.EscapeSqlQuotes(client.Name),
                        Points = _db.GetPlayerData<int>("points", client.XUID)
                    });

                _shopItems.CreateWeaponDataEntry(client);
            }

            // Set the players health and points
            CreatePlayerHud(client);

            // Update the high score huds
            UpdateHighScoreHuds();
        }

        /// <summary>
        /// Executes when a player spawns on the map
        /// </summary>
        /// <param name="client"></param>
        public override void OnPlayerSpawned(ServerClient client)
        {
            if (TestClients.IsBot(client)) return;

            if (!_playerData[client.XUID].Spawned)
            {
                _playerData[client.XUID].Spawned = true;
                Timing.AfterDelay(4000, () => iPrintLnBold(string.Format("^1Welcome to ^7{0}", GetDvar("sv_hostname")), client));
            }

            // Update the player hud
            UpdatePlayerHud(client);

            // Give the player their weapons back
            Timing.AfterDelay(500, client, () =>
                {
                    // Add the default map weapon id's
                    if (client.Team == Teams.Allies && !_shopItems.MapWeaonDataExists())
                    {
                        _shopItems.SetMapWeapons(new MapWeaponData
                            {
                                PrimaryAmmo = client.Ammo.PrimaryAmmo,
                                PrimaryAmmoClip = client.Ammo.PrimaryAmmoClip,
                                PrimaryWeapon = client.Other.PrimaryWeapon,
                                SecondaryAmmo = client.Ammo.SecondaryAmmo,
                                SecondaryAmmoClip = client.Ammo.SecondaryAmmoClip,
                                SecondaryWeapon = client.Other.SecondaryWeapon
                            });
                    }

                    // Get the players weapon data from the database and set it locally
                    _shopItems.UpdatePlayerWeaponsFromDb(client);

                    switch (client.Team)
                    {
                        case Teams.Allies:
                            _shopItems.SetPlayerWeapons(client);
                            break;

                        case Teams.Axis:
                            _shopItems.GiveInfectedRiotShield(client);
                            break;
                    }
                });
        }

        /// <summary>
        /// Executes when a player leaves the server
        /// </summary>
        /// <param name="client"></param>
        public override void OnPlayerDisconnect(ServerClient client)
        {
            if (TestClients.IsBot(client)) return;

            if (_playerStats.ContainsKey(client.ClientNum))
            {
                var healthHud = GetHudElement(_playerStats[client.ClientNum].Health);
                healthHud.Type = HudElementTypes.None;

                var pointsHud = GetHudElement(_playerStats[client.ClientNum].Points);
                pointsHud.Type = HudElementTypes.None;

                _playerStats.Remove(client.ClientNum);
            }

            // Update the players last online time
            _db.UpdatePlayerRecord(new ClientData
                {
                    TableData = new Dictionary<string, string>
                        {
                            {"points", _playerData[client.XUID].Points.ToString(CultureInfo.InvariantCulture)},
                            {"last_online", _db.DateTimeFormat(DateTime.Now)}
                        },
                    Xuid = client.XUID
                });

            // Reset the players spawn state
            _playerData[client.XUID].Spawned = false;

            // Update the high score huds
            UpdateHighScoreHuds();

            // Check if the bans file modified date has changed, if it has look inside for the players
            // so we can remove them from the database as they have been banned
            var lastModified = File.GetLastWriteTime(@"main\permanent.ban");
            if (lastModified.CompareTo(_bansLastUpdate) != 1) return;

            // Update the last modified time
            _bansLastUpdate = lastModified;

            // Get the contents of the bans file and search for the players XUID
            if (File.ReadAllLines(@"main\permanent.ban").Contains(client.XUID))
            {
                _db.DeletePlayer(client.XUID);
            }
        }

        /// <summary>
        /// Executes when a player recieves damage from either himself/herself or another player
        /// </summary>
        /// <param name="attacker"></param>
        /// <param name="victim"></param>
        /// <param name="weapon"></param>
        /// <param name="damage"></param>
        /// <param name="damageMod"></param>
        /// <param name="hitLocation"></param>
        /// <returns></returns>
        public override int OnPlayerDamaged(ServerClient attacker, ServerClient victim, string weapon, int damage,
                                            string damageMod, HitLocations hitLocation)
        {
            if (damage < victim.Other.Health || attacker.Team == victim.Team || victim.XUID == attacker.XUID)
            {
                return base.OnPlayerDamaged(attacker, victim, weapon, damage, damageMod, hitLocation);
            }

            var pointsToAdd = 100;

            // Extra points, this is based on the player getting a lot of damage against another
            // player, the more damage they inflict the more points they receive
            pointsToAdd = (int)(pointsToAdd + Math.Round((decimal)7 * (damage > 100 ? 100 : damage) / 5));

            // Did the player get a points bonus greater or equal to 400? If so let everyone on the
            // server know how good they are
            /*if (pointsToAdd >= 500 && _insaneKillTicker%15 == 0)
                {
                    ServerSay(string.Format("^2[SHOP] ^7{0} ^3got an ^7INSANE ^3points bonus", attacker.Name), true);
                }

                _insaneKillTicker++;*/

            // Is it a double points weekend?
            if (_doublePoints)
            {
                pointsToAdd = pointsToAdd * 2;
            }

            // Update the players total number of points and their hud
            _playerData[attacker.XUID].Points += pointsToAdd;
            iPrintLnBold(string.Format("+{0}", pointsToAdd), attacker);

            // Update the players points in the database
            _db.UpdatePlayerPoints(attacker.XUID, _playerData[attacker.XUID].Points);

            // Check how many humans are alive, if none are delete all the hud elements
            if (TeamCount(Team.Allies) <= 1)
            {
                Timing.AfterDelay(8000, () => RemoveAllPlayerHuds());
            }
            else
            {
                UpdatePlayerHud(attacker);
                UpdateHighScoreHuds();
            }

            return base.OnPlayerDamaged(attacker, victim, weapon, damage, damageMod, hitLocation);
        }

        /// <summary>
        /// Executes just before the game map changes
        /// </summary>
        public override void OnPreMapChange()
        {
            _shopItems.ResetRiotShields();
        }

        /// <summary>
        /// Executes when the game map changes
        /// </summary>
        public override void OnMapChange()
        {
            _counter = 0;
            ResetClientData(true);
        }

        /// <summary>
        /// Executes when the current map restarts
        /// </summary>
        public override void OnFastRestart()
        {
            _counter = 0;
            ResetClientData(true);
        }

        /// <summary>
        /// Executes when a player types something in the chat
        /// </summary>
        /// <param name="message"></param>
        /// <param name="client"></param>
        /// <param name="teamchat"></param>
        /// <returns></returns>
        public override ChatType OnSay(string message, ServerClient client, bool teamchat)
        {
            ShopItem item = null;
            ShopDetails details = null;
            const string invalidTeam = "^1You can't use weapons as an infected player!";

            // Split the message
            var msg = message.Split(' ');

            // Ensure the command is valid and part of our commands list
            var command = msg[0] ?? "unknown";

            if (command == "unknown" || !Regex.Match(command, @"(\/|!)").Success ||
                !_commands.Contains(command.Substring(1).ToLower()))
            {
                return ChatType.ChatContinue;
            }

            // Get the players points
            var points = _playerData[client.XUID].Points;

            switch (command)
            {
                case "/info":
                    iPrintLnBold(
                        "^4Welcome ^7to ^1|^7RR^1|Infected ^7HJ ^4SHOP^1! Kill people to receive points and use them in the shop!",
                        client);
                    break;

                case "/help":
                    iPrintLnBold("^2Use /shop to see the shop!", client);
                    break;

                case "/points":
                case "/mp":
                case "/point":
                    TellClient(client.ClientNum,
                               string.Format("^2You have ^3{0} ^2points", Helpers.FormatPoints(points)), true);
                    break;

                case "/shop":
                    details = new ShopDetails
                        {
                            AlliesMessage =
                                string.Format("^1/ammo ^7- ^3buy weapon ammo ^7| ^1/weapons ^7- ^3shows weapon list"),
                            AxisMessage =
                                _shopItems.GetItemsFormatted(_shopItems.GetAdvertisedItems(Teams.Axis), " ^7| ^3")
                        };
                    break;

                case "/weapons":
                    details = new ShopDetails
                        {
                            AlliesMessage = _shopItems.GetShopCommands(),
                            AxisMessage = invalidTeam
                        };
                    break;

                case "/smg":
                case "/lmg":
                case "/assault":
                case "/snipe":
                case "/shotguns":
                case "/expl":
                case "/handgun":
                    details = new ShopDetails
                        {
                            AlliesMessage = _shopItems.GetItemsFormatted(command.Substring(1)),
                            AxisMessage = invalidTeam
                        };
                    break;

                case "/resetweapon":
                case "/resetweapons":
                    if (client.Team != Teams.Allies)
                    {
                        TellClient(client.ClientNum, string.Format("^2[SHOP] ^:Only humans can reset their weapons"), true);
                        return ChatType.ChatNone;
                    }

                    _shopItems.ResetPlayerWeapons(client, command == "/resetweapons");
                    break;

                case "!dp":
                    _playerData[client.XUID].Points += 5000;
                    iPrintLnBold(string.Format("^5You now have ^1{0} ^5points", GetPlayerPoints(client)), client);
                    UpdatePlayerHud(client);

                    // Log this to the server log
                    ServerPrint(string.Format("[SHOP] {0} has given himself/herself 5000 points", client.Name));

                    // Update the high score huds
                    UpdateHighScoreHuds();

                    // Update the players points in the database
                    _db.UpdatePlayerPoints(client.XUID, _playerData[client.XUID].Points);
                    break;

                case "!givepoints":
                    GiveOrTakePointsFromPlayer(msg, client);
                    break;

                case "!takepoints":
                    GiveOrTakePointsFromPlayer(msg, client);
                    break;

                case "!spyon":
                    SpyOnPlayerPoints(msg, client);
                    break;

                case "!sendpoints":
                    DetermineSendPointsStatus(msg, client);
                    break;

                default:
                    item = _shopItems.DetermineShopItem(command.Substring(1));

                    if (item == null)
                    {
                        return ChatType.ChatContinue;
                    }
                    break;
            }

            // Were there any message details set?
            if (details != null)
            {
                iPrintLnBold(client.Team == Teams.Axis ? details.AxisMessage : details.AlliesMessage, client);
            }

            // Check if an item was selected
            if (item == null)
            {
                return ChatType.ChatNone;
            }

            if (item.Teams.Contains(client.Team))
            {
                var boughtMessage = item.BoughtMessage;
                var itemPrice = item.Price;

                // Determine the players current weapon
                var currentWeapon = client.Other.CurrentWeapon == client.Other.PrimaryWeapon ? "PrimaryAmmoClip" : "SecondaryAmmoClip";

                // Is the ammo command being used? If so update the item price by getting the real shop item
                if (item.Command == "ammo")
                {
                    var weapon = _shopItems.GetItemByNameUsingCurrentWeapon(client);

                    if (weapon == null)
                    {
                        TellClient(client.ClientNum, "^2[SHOP] ^:You can't buy ammo for a non-store bought weapon", true);
                        return ChatType.ChatNone;
                    }

                    if (weapon.Ammo != null)
                    {
                        itemPrice = weapon.Ammo.Price;

                        // Get the players current ammo clip size and the weapon ammo data
                        var ammoClipAmount = (int) typeof (Ammo_t).GetProperty(currentWeapon).GetValue(client.Ammo, null);
                        var ammo = weapon.Ammo;
                        var ammoAmount = ammo.PurchaseAmount;
                        var total = ammoClipAmount + ammoAmount;
                        var purchaseAmount = ammoAmount;
                        var maxAmmo = ammo.MaxClipSize;
                        decimal discount = 0;

                        // Check the players last ammount of ammo compared to now, if they only purchased a percentage
                        // of ammo deduct a percentage of the price
                        if (total >= maxAmmo)
                        {
                            // Set the purchase ammount
                            purchaseAmount = maxAmmo - ammoClipAmount;

                            // Update the discount total
                            discount = 100 - Math.Floor(((decimal) purchaseAmount/ammoAmount)*100);

                            // Update the item price
                            //itemPrice = itemPrice - (int) discount*2;
                            SetAmmoPriceForPurchaseAmount(purchaseAmount, ref itemPrice);
                        }

                        boughtMessage = string.Format(boughtMessage, purchaseAmount, itemPrice, discount);
                    }
                    else
                    {
                        TellClient(client.ClientNum, "^2[SHOP] ^:You can't buy ammo for this item", true);
                        return ChatType.ChatNone;
                    }
                }

                // Now the fun part, determine what we need to update and also ensure the player has enough
                // points to buy the item
                if (points < itemPrice)
                {
                    iPrintLnBold("^1You don't have enough points, ^7that sucks!", client);
                }
                else
                {
                    var itemExecute = new List<ItemExec>
                    {
                        _shopItems.GetWeaponByNameAndSetPrimaryWeapon,
                        _shopItems.SetPrimaryAndSecondaryAmmoAndClips,
                        _shopItems.GetWeaponByNameAndSetEquipment,
                        _shopItems.GetWeaponByNameAndSetOffHand,
                        _shopItems.SetPlayerOtherSpecials,
                        _shopItems.SetPlayerModel,
                        _shopItems.SetPlayerPerk
                    };

                    // Did something go wrong during the process?
                    var cmd = command.Substring(1);
                    foreach (var status in itemExecute.Select(fn => fn(item, ref client, cmd)).Where(status => status != null))
                    {
                        TellClient(client.ClientNum, string.Format("^2[SHOP] ^:{0}", status), true);
                        return ChatType.ChatNone;
                    }

                    // Deduct the points from the player and let the player know they purchased the item successfully
                    _playerData[client.XUID].Points -= itemPrice;

                    iPrintLnBold(
                        boughtMessage ??
                        string.Format("^2Item purchased, you now have ^3{0} ^2points",
                            Helpers.FormatPoints(_playerData[client.XUID].Points)), client);

                    UpdatePlayerHud(client);

                    // Update the players points in the database
                    _db.UpdatePlayerPoints(client.XUID, _playerData[client.XUID].Points);
                }
            }
            else
            {
                iPrintLnBold("^1Sorry, you can't buy that item", client);
            }

            return ChatType.ChatNone;
        }

        /// <summary>
        /// Returns a price based on the amount of bullets that are being purchased
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="price"></param>
        private static void SetAmmoPriceForPurchaseAmount(int amount, ref int price)
        {
            if (amount == 1)
            {
                price = 1;
            }
            else if (amount > 1 && amount < 10)
            {
                price = amount * 2 - 1;
            }
            else if (amount > 10 && amount < 20)
            {
                price = amount * 3 - 8;
            }
            else if (amount > 20 && amount < 30)
            {
                price = amount * 4;
            }
            else if (amount > 30 && amount < 40)
            {
                price = amount * 6;
            }
            else if (amount > 40)
            {
                price = amount * 8;
            }
        }

        /// <summary>
        /// Either gives points to a player or takes points from one based on the given command, if the
        /// command is to give points prevent the issuer from giving points to himself/herself as that
        /// will be considered cheating if they try
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="issuer"></param>
        private void GiveOrTakePointsFromPlayer(IList<string> msg, ServerClient issuer)
        {
            int pointsToGiveOrTake;

            // Get the command that was used
            var command = msg[0];

            if (msg.Count < 3 || msg[1] == null || msg[2] == null)
            {
                TellClient(issuer.ClientNum, string.Format("^2[SHOP] ^:{0} <player> <points>", command), true);
            }
            else if (!int.TryParse(msg[2], out pointsToGiveOrTake))
            {
                TellClient(issuer.ClientNum, "^2[SHOP] ^1Please enter a valid number for the points", true);
            }
            else if (pointsToGiveOrTake > 0)
            {
                var client = MatchClientByName(msg[1].ToLower());

                if (client != null)
                {
                    if (command == "!givepoints")
                    {
                        GivePointsToPlayer(client, issuer, pointsToGiveOrTake);
                    }
                    else
                    {
                        TakePointsFromPlayer(client, issuer, pointsToGiveOrTake);
                    }
                }
                else
                {
                    TellClient(issuer.ClientNum, "^2[SHOP] ^:Looks like the client wasn't found, well that sucks.", true);
                }
            }
            else
            {
                TellClient(issuer.ClientNum,
                    "^2[SHOP] ^1Please enter a valid number for the points, must be greater than 0", true);
            }
        }

        /// <summary>
        /// Gives points to the player that was found based on the message
        /// </summary>
        /// <param name="client"></param>
        /// <param name="issuer"></param>
        /// <param name="pointsToGive"></param>
        private void GivePointsToPlayer(ServerClient client, ServerClient issuer, int pointsToGive)
        {
            if (client.XUID == issuer.XUID)
            {
                iPrintLnBold("^2Stop trying to cheat", issuer);
                return;
            }

            _playerData[client.XUID].Points += pointsToGive;

            // Update the players points in the database
            _db.UpdatePlayerPoints(client.XUID, _playerData[client.XUID].Points);

            // Let the client know they receieved points
            TellClient(client.ClientNum,
                string.Format("^2[SHOP] ^7You have receieved ^2{0} ^7points from ^:{1}",
                    Helpers.FormatPoints(pointsToGive), issuer.Name), true);

            TellClient(issuer.ClientNum,
                string.Format("^2[SHOP] ^:{0} ^7has received ^2{1} ^7points", client.Name,
                    Helpers.FormatPoints(pointsToGive)), true);

            // Update the high score huds
            UpdateHighScoreHuds();

            // Update the players hud
            UpdatePlayerHud(client);
        }

        /// <summary>
        /// Takes points to the player that was found based on the message
        /// </summary>
        /// <param name="client"></param>
        /// <param name="issuer"></param>
        /// <param name="pointsToTake"></param>
        private void TakePointsFromPlayer(ServerClient client, ServerClient issuer, int pointsToTake)
        {
            // Update the users points total
            _playerData[client.XUID].Points -= pointsToTake;

            // Update the players points in the database
            _db.UpdatePlayerPoints(client.XUID, _playerData[client.XUID].Points);

            // Let the client know they've had points taken away
            TellClient(client.ClientNum,
                string.Format("^2[SHOP] ^7You've just had ^2{0} ^7points taken from you",
                    Helpers.FormatPoints(pointsToTake)), true);

            TellClient(issuer.ClientNum,
                       string.Format("^2[SHOP] ^:You have taken ^2{0} ^7points from ^:{1}", Helpers.FormatPoints(pointsToTake),
                                     client.Name), true);

            // Update the high score huds
            UpdateHighScoreHuds();

            // Update the players hud
            UpdatePlayerHud(client);
        }

        /// <summary>
        /// Allows the issuer to spy on another players points
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="issuer"></param>
        private void SpyOnPlayerPoints(IList<string> msg, ServerClient issuer)
        {
            if (msg.Count == 1 || string.IsNullOrEmpty(msg[1]))
            {
                TellClient(issuer.ClientNum, "^2[SHOP] ^:!spyon <player>", true);
            }
            else
            {
                var client = MatchClientByName(msg[1].ToLower());

                TellClient(issuer.ClientNum,
                           client != null
                               ? string.Format("^2[SHOP] ^7{0} ^:has ^2{1} ^:points", client.Name, GetPlayerPoints(client))
                               : "^2[SHOP] ^:Looks like the client wasn't found, well that sucks.",
                           true);
            }
        }

        /// <summary>
        /// Determines if the player that issued this command is wanting to send points to
        /// another player and/or confirm or cancel the sending of points
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="issuer"></param>
        private void DetermineSendPointsStatus(IList<string> msg, ServerClient issuer)
        {
            var message = "";
            var points = 0;
            var playerSendPoints = _playerData[issuer.XUID].SendPoints;

            if (playerSendPoints.IsSending)
            {
                if (msg.Count == 1 || (msg[1] != null && !Regex.Match(msg[1], @"(confirm|cancel)").Success))
                {
                    message = "^:!sendpoints <confirm or cancel>";
                }
                else
                {
                    switch (msg[1])
                    {
                        case "confirm":
                            message = string.Format("^7You have successfully given ^3{0} ^7points to ^2{1}",
                                                    playerSendPoints.Points, playerSendPoints.SendingTo.Name);

                            // Update the recieving players total points and the current players total points
                            var sendToXuid = playerSendPoints.SendingTo.XUID;

                            _playerData[sendToXuid].Points += playerSendPoints.Points;
                            _playerData[issuer.XUID].Points -= playerSendPoints.Points;
                            
                            // Let the player know they got points
                            TellClient(playerSendPoints.SendingTo.ClientNum,
                                       string.Format("^2[SHOP] ^:You've just received ^7{0} ^:points from ^7{1}",
                                                     Helpers.FormatPoints(playerSendPoints.Points), issuer.Name), true);

                            // Update the players huds
                            UpdatePlayerHud(issuer);
                            UpdatePlayerHud(playerSendPoints.SendingTo);

                            // Update the players points in the database
                            _db.UpdatePlayerPoints(sendToXuid, _playerData[sendToXuid].Points);
                            _db.UpdatePlayerPoints(issuer.XUID, _playerData[issuer.XUID].Points);
                            break;

                        case "cancel":
                            message = string.Format("^:You have canceled trading between ^7yourself ^:and ^7{0}",
                                playerSendPoints.SendingTo.Name);
                            break;
                    }

                    // Reset the players trading status
                    playerSendPoints.Reset();
                }
            }
            else if (msg.Count == 1 || string.IsNullOrEmpty(msg[1]))
            {
                message = "^:!sendpoints <player> <points>";
            }
            else if (msg.Count == 2 || (msg.Count == 3 && !int.TryParse(msg[2], out points)) || points <= 0)
            {
                message = "^7Please enter a valid number that's greater than 0";
            }
            else
            {
                var client = MatchClientByName(msg[1].ToLower());

                if (client == null)
                {
                    message = "^:Looks like the player wasn't found, well that sucks.";
                }
                else if (client.XUID == issuer.XUID)
                {
                    message = "^:You can't send points to yourself, A+ for effort though";
                }
                else
                {
                    message = "^7Please type ^:!sendpoints confirm ^7to finish sending your points otherwise type ^:!sendpoints cancel";

                    // Set the sending status
                    playerSendPoints.IsSending = true;
                    playerSendPoints.Points = points;
                    playerSendPoints.SendingTo = client;
                }
            }

            TellClient(issuer.ClientNum, string.Format("^2[SHOP] {0}", message), true);
        }

        /// <summary>
        /// Attempts to match the given string against all the clients on the server
        /// </summary>
        /// <param name="find"></param>
        /// <returns></returns>
        private ServerClient MatchClientByName(string find)
        {
            find = find.ToLower();
            return GetClients().FirstOrDefault(x => x.Name.ToLower() == find || x.Name.ToLower().Contains(find));
        }

        /// <summary>
        /// Updates the help huds for the humans weapons list
        /// </summary>
        private bool UpdateWeaponHelpHuds()
        {
            try
            {
                var parents = new List<string> {"allies", "axis"};

                foreach (var parent in parents)
                {
                    // Get all the shop items for the parent
                    var items =
                        GetRandomItems(parent == "allies" ? _shopItemsHud.Allies : _shopItemsHud.Axis) as List<ItemInfo>;

                    if (items == null || items.Count < 5)
                    {
                        break;
                    }

                    // Get all the huds and update them
                    var huds = _helpHuds[parent].Select(GetHudElement).ToList();
                    huds.RemoveAt(0);

                    for (var i = 0; i < huds.Count; i++)
                    {
                        if (huds[i] == null) continue;

                        // Get the item reference
                        var item = items[i];

                        // Get the item price and determine if it needs to be spaced to sit inline with prices
                        // that are 4 characters long
                        var itemPrice = item.Points.ToString(CultureInfo.InvariantCulture);

                        if (itemPrice.Length == 3)
                        {
                            itemPrice = "   " + itemPrice;
                        }

                        // Update the hud
                        huds[i].SetString(string.Format("^7[^1{0}^7] {1} ^:/{2}",
                            (item.Points != -1 ? itemPrice : "  xxxx"), item.Name.ToUpper(),
                            item.Command));
                    }
                }
            }
            catch
            {
                // For some odd reason this decides to error out randomly even though there are checks above
                // to prevent exceptions from occurring, if you fix this please let SgtLegend know so he can
                // buy you a nice big cookie
            }

            return true;
        }

        /// <summary>
        /// Picks 5 random items from the items list that was setup earlier on
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        private static IEnumerable<ItemInfo> GetRandomItems(List<ItemInfo> items)
        {
            var randomNumber = new Random(DateTime.Now.Millisecond);
            var n = items.Count;

            while (n > 1)
            {
                n--;

                // Get the next random number
                var k = randomNumber.Next(n + 1);

                // Get the random shop item
                var value = items[k];

                // Now re-arrange the item
                items[k] = items[n];
                items[n] = value;
            }
            
            return items.GetRange(0, 5);
        }

        /// <summary>
        /// Determines if double points weekend should be active and also checks the last time
        /// everyone's points were reset, if 2 months have passed everyone should be reset back
        /// to 0 which keeps it fair
        /// </summary>
        private bool CheckPointsStatus()
        {
            try
            {
                SetDoublePointsWeekendStatus();
                CheckIfPlayerPointsNeedToBeReset();

                // Display a message that lets players know points are reset every 2 months
                if (_pointsTicker%3 == 0)
                {
                    ServerSay("^2[SHOP] ^7Every 2 months points are reset automatically, this is to keep it fair", true);
                }

                // Display a message that lets players know that they can give points to other players
                if (_pointsTicker%5 == 0)
                {
                    ServerSay("^2[SHOP] ^7You can send points to other players by using ^:!sendpoints", true);
                }

                _pointsTicker++;
            }
            catch
            {
                // For some odd reason this decides to error out randomly even though there are checks above
                // to prevent exceptions from occurring, if you fix this please let SgtLegend know so he can
                // buy you a nice big cookie
            }

            return true;
        }

        /// <summary>
        /// Determines which day it is and what the time if and activates/deactivates double the
        /// points weekend based on the server date/time
        /// </summary>
        private void SetDoublePointsWeekendStatus()
        {
            var time = DateTime.Now;

            _doublePoints = (time.DayOfWeek == DayOfWeek.Saturday && time.Hour >= 0 && time.Minute >= 0) ||
                            (time.DayOfWeek == DayOfWeek.Sunday && time.Hour <= 11 && time.Minute <= 59);

            // If the ticker is a percentage of 0 display a message to everyone on the server
            if (_pointsTicker%4 == 0 && _doublePoints)
            {
                ServerSay(
                    "^2[SHOP] ^7It's a double points weekend, get a points bonus and receive up to 1000+ points for one kill",
                    true);
            }
        }

        /// <summary>
        /// Reads the player points from the classic points text file and converts the data into
        /// SQLite table records, upon the conversion finishing the "player_points.txt" files is
        /// removed from the server as it's not needed anymore
        /// </summary>
        private void LoadAndConvertPlayerPoints()
        {
            const string playerPointsFiles = "player_points.txt";

            try
            {
                var points = File.ReadAllLines(playerPointsFiles);

                foreach (var p in from player in points
                                  where !string.IsNullOrEmpty(player)
                                  select player.Split('=')
                                  into p where !_db.PlayerExists(p[0]) select p)
                {
                    if (_pointsThreadTicker > 0 && _pointsThreadTicker%20 == 0)
                    {
                        Thread.Sleep(500);
                    }

                    _db.CreatePlayerRecord(new ClientData
                        {
                            Name = p.Length > 2 ? p[2] : "Player_Unknown",
                            Points = int.Parse(p[1]),
                            Xuid = p[0]
                        });

                    _pointsThreadTicker++;
                }

                // Make a copy of the points file
                File.Move(playerPointsFiles, playerPointsFiles.Replace(".t", "_backup.t"));

                // Delete the points file
                File.Delete(playerPointsFiles);

                // Display a message in the server console
                ServerPrint("\n[SHOP] Successfully read the player points from disk and converted them into SQLite records.");
                ServerPrint("[SHOP] The 'player_points.txt' file has been renamed to 'player_points_backup.txt' in case something did go wrong.\n");
            }
            catch (Exception ex)
            {
                ServerPrint("An error occurred while attempting to read the contents of the player points file");
                ServerPrint(ex.ToString());
            }
        }

        /// <summary>
        /// Loads the shop info data from the HDD
        /// </summary>
        private void LoadShopInfoFromDisk()
        {
            if (!File.Exists("shop_info.txt"))
            {
                _infoData = new Dictionary<string, string>
                    {
                        {"last_reset", DateTime.Now.AddMonths(-2).ToString(CultureInfo.InvariantCulture)}
                    };

                SaveShopInfoDataToDisk();
            }

            var infoData = File.ReadAllLines("shop_info.txt");

            foreach (var data in infoData.Select(info => info.Split('=')))
            {
                _infoData[data[0]] = data[1];
            }
        }

        /// <summary>
        /// Saves all the shop info data to the disk
        /// </summary>
        private void SaveShopInfoDataToDisk()
        {
            File.WriteAllLines("shop_info.txt",
                               _infoData.Select(x => string.Format("{0}={1}", x.Key, x.Value)).ToArray());
        }

        /// <summary>
        /// Checks whether everyone's points need to be reset, this happens once every 2 months
        /// which ensures everything stays fair
        /// </summary>
        private void CheckIfPlayerPointsNeedToBeReset()
        {
            try
            {
                var lastResetDate = DateTime.Parse(_infoData["last_reset"]);
                var currentDate = DateTime.Now;
                var lastReset = Math.Round(currentDate.Subtract(lastResetDate).Days/(365.25/12));

                if (!(lastReset >= 2)) return;
                _infoData["last_reset"] = DateTime.Now.ToString(CultureInfo.InvariantCulture);

                foreach (var player in _playerData)
                {
                    player.Value.Points = 0;

                    // Update the players points in the database
                    _db.UpdatePlayerPoints(player.Key, player.Value.Points);
                }

                // Trigger the save functionality
                SaveShopInfoDataToDisk();
            }
            catch (Exception ex)
            {
                var frame = new StackTrace(ex, true).GetFrame(0);

                ServerLog(LogType.LogConsole,
                          string.Format("[Caught Exception] An error has occurred on line ( {0} ) of ( {1} )",
                                        frame.GetFileLineNumber(), frame.GetFileName()));
            }
        }

        /// <summary>
        /// Creates a new hud element on the players screen
        /// </summary>
        /// <param name="showTo"></param>
        /// <param name="message"></param>
        /// <param name="originX"></param>
        /// <param name="originY"></param>
        /// <param name="team"></param>
        /// <param name="alignOrg"></param>
        /// <param name="pointType"></param>
        /// <returns></returns>
        private int CreateHudElement(int showTo, string message, float originX, float originY, Teams team = Teams.FFA,
                                     int alignOrg = 0, int pointType = 100)
        {
            var hud = CreateNewHudElem();

            // Do we need to set a team, only set one if the enum is not equal to FFA
            if (team != Teams.FFA)
            {
                hud.Team = team;
            }

            hud.AlignOrg = (byte) alignOrg;
            hud.Color.GlowA = 20;
            hud.Color.GlowG = 20;
            hud.Font = HudElementFonts.HudMedium;
            hud.FontScale = 1f;
            hud.HideInMenu = true;
            hud.OriginX = originX;
            hud.OriginY = originY;
            hud.PointType = (byte) pointType;
            hud.ShowToEnt = showTo;
            hud.Type = HudElementTypes.Text;
            hud.SetString(message);

            return hud.HudElementNum;
        }

        /// <summary>
        /// Creates a single player hud
        /// </summary>
        /// <param name="player"></param>
        private void CreatePlayerHud(ServerClient player)
        {
            var healthHud = CreateHudElement(player.ClientNum, string.Format("^:Health: ^2{0}", player.Other.Health), 22f, 125f);
            var pointsHud = CreateHudElement(player.ClientNum, string.Format("^:Points: ^7{0}", GetPlayerPoints(player)), 22f, (125f + 12));

            if (_playerStats.ContainsKey(player.ClientNum))
            {
                _playerStats[player.ClientNum].Health = healthHud;
                _playerStats[player.ClientNum].Points = pointsHud;
            }
            else
            {
                _playerStats.Add(player.ClientNum, new PlayerHuds
                    {
                        Health = healthHud,
                        Points = pointsHud
                    });
            }
        }

        /// <summary>
        /// Updates the players hud with new information about their health and points
        /// </summary>
        /// <param name="client"></param>
        private void UpdatePlayerHud(ServerClient client)
        {
            if (TestClients.IsBot(client)) return;

            //var healthHud = GetHudElement(_playerStats[client.ClientNum].Health);
            var pointsHud = GetHudElement(_playerStats[client.ClientNum].Points);

            if (pointsHud != null)
            {
                //healthHud.SetString(string.Format("^:Health: ^2{0}", client.Other.Health));
                pointsHud.SetString(string.Format("^:Points: ^7{0}", GetPlayerPoints(client)));
            }
        }

        /// <summary>
        /// Removes all the player huds
        /// </summary>
        /// <param name="complete"></param>
        private void RemoveAllPlayerHuds(bool complete = false)
        {
            try
            {
                foreach (var client in GetClients().Where(x => !TestClients.IsBot(x)))
                {
                    if (!_playerStats.ContainsKey(client.ClientNum)) continue;

                    var healthHud = GetHudElement(_playerStats[client.ClientNum].Health);
                    healthHud.Type = HudElementTypes.None;
                    healthHud.SetString("");

                    var pointsHud = GetHudElement(_playerStats[client.ClientNum].Points);
                    pointsHud.Type = HudElementTypes.None;
                    pointsHud.SetString("");
                }

                foreach (var hud in _helpHuds.SelectMany(huds => huds.Value).Select(GetHudElement))
                {
                    hud.Type = HudElementTypes.None;
                    hud.SetString("");
                }

                foreach (var hud in _topPoints.Select(GetHudElement))
                {
                    hud.Type = HudElementTypes.None;
                    hud.SetString("");
                }
            }
            catch (Exception ex)
            {
                ServerLog(LogType.LogConsole, "An error occurred while attempting to remove the huds\n");
                ServerLog(LogType.LogConsole, ex.ToString());
            }

            // If the "complete" paramter is set to true clear all the hud dictionaries and lists
            if (complete)
            {
                _playerStats.Clear();
                _helpHuds.Clear();
                _topPoints.Clear();
            }
            else
            {
                Timing.AfterDelay(500, () => RemoveAllPlayerHuds(true));
            }
        }

        /// <summary>
        /// Orders the points for every player which puts the highest scoring players at the top
        /// </summary>
        private void CalculateTopPlayers()
        {
            var points = _playerData.OrderByDescending(x => x.Value.Points);

            // Slice down the top 5 players
            var total = 0;

            _topPlayers.Clear();
            foreach (var player in points)
            {
                if (total >= 5)
                {
                    break;
                }

                if (player.Value.Points <= 0) continue;
                _topPlayers.Add(GetLatestNameForXuid(player.Key, player.Value.Name));
                total++;
            }
        }

        /// <summary>
        /// Attempts to find the players latest name through the online players, if the player
        /// can't be found a fallback will be used
        /// </summary>
        /// <param name="xuid"></param>
        /// <param name="fallback"></param>
        /// <returns></returns>
        private string GetLatestNameForXuid(string xuid, string fallback)
        {
            var client = GetClients().Where(x => !TestClients.IsBot(x)).FirstOrDefault(x => x.XUID == xuid);
            return client != null ? client.Name : fallback;
        }

        /// <summary>
        /// Creates additional huds that are helpful to users
        /// </summary>
        private void CreateHelpHuds()
        {
            if (_helpHuds.Count > 0)
            {
                foreach (var hud in from huds in _helpHuds.Values from hudNum in huds select GetHudElement(hudNum))
                {
                    hud.Type = HudElementTypes.None;
                }

                _helpHuds.Clear();
            }

            // How to access the shop
            _helpHuds["misc"] = new List<int>
                {
                    CreateHudElement(Entity_World, "^7Type ^2/shop ^7to access the shop", 22f, 110f)
                };

            // Shop items for human and infected players
            _helpHuds["allies"] = GenerateTeamItemsHuds(_shopItemsHud.Allies, Teams.Allies, "^2WHAT TO BUY?");
            _helpHuds["axis"] = GenerateTeamItemsHuds(_shopItemsHud.Axis, Teams.Axis, "^3WHAT TO BUY?");
        }

        /// <summary>
        /// Generates a set of huds for for shop items that can be randomized later on
        /// </summary>
        /// <param name="items"></param>
        /// <param name="team"></param>
        /// <param name="headingText"></param>
        /// <returns></returns>
        private List<int> GenerateTeamItemsHuds(IEnumerable<ItemInfo> items, Teams team, string headingText)
        {
            var huds = new List<int>();
            var counter = 0;

            // Create the heading hud
            huds.Add(CreateHudElement(Entity_World, headingText, 120f, 3f, team, 2000, 0));

            foreach (var item in items)
            {
                if (counter == 5)
                {
                    break;
                }

                var itemPrice = item.Points.ToString(CultureInfo.InvariantCulture);

                if (itemPrice.Length == 3)
                {
                    itemPrice = "   " + itemPrice;
                }

                // Create the item hud
                huds.Add(CreateHudElement(Entity_World,
                                          string.Format("^7[^1{0}^7] {1} ^:/{2}",
                                                        (item.Points != -1 ? itemPrice : "  xxxx"), item.Name.ToUpper(),
                                                        item.Command), 120f, (3f + (15 * (counter + 1))), team, 2000, 0));

                counter++;
            }

            return huds;
        }

        /// <summary>
        /// Gets the top player name "if any" and formats the text
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private string HighScoreHudText(int index)
        {
            return string.Format("^1(^7{0}^1) ^7{1}", index, GetTopPlayer(index - 1));
        }

        /// <summary>
        /// Retrieves the top player for the given integer, if no player exists no name is
        /// returned
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        private string GetTopPlayer(int index)
        {
            var name = (_topPlayers.Count > index && _topPlayers[index] != "Player_Unknown")
                              ? _topPlayers[index]
                              : "^8-";

            // Do we need to concatenate the players name?
            if (name.Length > 18)
            {
                name = name.Substring(0, 18) + "...";
            }

            return name;
        }

        /// <summary>
        /// Creates huds for the highest scoring players on the server
        /// </summary>
        private void CreateHighScoreHuds()
        {
            // Before we continue lets ensure all the previous huds are gone
            if (_topPoints.Count > 0)
            {
                foreach (var hud in _topPoints.Select(GetHudElement))
                {
                    hud.Type = HudElementTypes.None;
                }

                _topPoints.Clear();
            }

            // Create the heading "Top 5 players"
            _topPoints.Add(CreateHudElement(Entity_World, "^7TOP 5 PLAYERS", 6f, 3f, Teams.FFA, 2000, 0));

            // Create the top 5 huds
            for (var i = 1; i < 6; i++)
            {
                _topPoints.Add(CreateHudElement(Entity_World, HighScoreHudText(i), 6f, 3f + (15 * i), Teams.FFA, 2000, 0));
            }
        }

        /// <summary>
        /// Updates all the high score huds expect for the "Top 5 players" hud
        /// </summary>
        private void UpdateHighScoreHuds()
        {
            CalculateTopPlayers();

            for (var i = 1; i < 6; i++)
            {
                var hud = GetHudElement(_topPoints[i]);
                hud.SetString(HighScoreHudText(i));
            }
        }

        /// <summary>
        /// Retrieves and formats the players points into a comma seperated value
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        private string GetPlayerPoints(ServerClient player)
        {
            var points = _playerData.ContainsKey(player.XUID) ? _playerData[player.XUID].Points : 0;
            return Helpers.FormatPoints(points);
        }

        /// <summary>
        /// Resets the client HUD's and the player spawn states
        /// </summary>
        /// <param name="resetData"></param>
        private void ResetClientData(bool resetData)
        {
            if (!resetData) return;

            try
            {
                // Reset the player spawn states
                foreach (var client in GetClients().Where(x => !TestClients.IsBot(x)))
                {
                    _playerData[client.XUID].Spawned = false;
                }
            }
            catch (Exception ex)
            {
                var frame = new StackTrace(ex, true).GetFrame(0);

                ServerLog(LogType.LogConsole,
                          string.Format("[Caught Exception] An error has occurred on line ( {0} ) of ( {1} )",
                                        frame.GetFileLineNumber(), frame.GetFileName()));
            }
        }
    }

    /// <summary>
    /// Simply holds data about the player
    /// </summary>
    internal class PlayerData
    {
        public string           Name            = "Player_Unknown";
        public int              Points;
        public SendStatus       SendPoints      = new SendStatus();
        public bool             Spawned         = false;
    }

    /// <summary>
    /// Simply holds the data for the player huds
    /// </summary>
    internal class PlayerHuds
    {
        public int Health, Points;
    }

    /// <summary>
    /// Holds information about the players trading status
    /// </summary>
    internal class SendStatus
    {
        public int Points;
        public bool IsSending;
        public ServerClient SendingTo;

        public void Reset()
        {
            Points = 0;
            IsSending = false;
            SendingTo = null;
        }
    }
}