﻿using System.ComponentModel;

namespace CustomShop
{
    internal enum Prices
    {
        // Kits for survivors
        Lightweightkit      = 8765,
        Heavykit            = 9001,

        // SMG
        Mp5                 = 620,
        Mp7                 = 820,
        M9                  = 750,
        P90                 = 900,
        Pp90M1              = 1100,
        Ump45               = 1100,
        
        // Handguns
        Magnum44            = 600,
        Usp45               = 500,
        DesertEagle         = 750,
        Mp412               = 650,
        P99                 = 500,
        FiveSeven           = 550,

        // Light Machine Guns
        M60                 = 1200,
        Mk46                = 1300,
        Pech                = 1400,
        Sa80                = 1500,
        Mg36                = 1600,

        // Assualt
        M4                  = 1200,
        Ak47                = 1800,
        M16                 = 1400,
        Fad                 = 1500,
        Acr                 = 1600,
        Mk14                = 1700,
        Scar                = 1600,
        G36C                = 1900,
        Cm901               = 1800,

        // Sniper Rifles
        Barret              = 1500,
        Rsass               = 1400,
        Dragunov            = 1400,
        Msr                 = 1300,
        L96A1               = 1400,
        As50                = 1500,

        // Shotguns
        Model               = 1000,   // 1887
        Aa12                = 1100,
        Usas12              = 1200,
        Spas12              = 1300,
        Striker             = 1400,
        Ksg                 = 1400,

        // Explosives
        Rpg                 = 5000,
        Xm25                = 4000,
        Stinger             = 4000,
        Smaw                = 4500,
        Claymore            = 1000,
        ThrowingKnife       = 250,

        // Misc.
        Ammo                = 500,
        Smoke               = 1000,
        Flash               = 1500,
        Tk                  = 2500,

        // Perks
        Fm                  = 400,    // Fastermelee
        Fr                  = 500,    // Fastreload
        Sc                  = 600,    // Scavenger
        Be                  = 600,    // Blindeye
        Cb                  = 600,    // Cold blooded
        Ba                  = 700,    // Bullet accuracy
        Sk                  = 600,    // Stalker

        // Infected items
        Emp                 = 500,
        Shield              = 2500,
        UavCost             = 1000,
        JuicedCost          = 1000,
        Health              = 500
    }

    internal enum ItemCategory
    {
        [Description("SMGs/smg")] Smg,
        [Description("LMGs/lmg")] Lmg,
        [Description("Assualt/assualt")] Assualt,
        [Description("Snipers/snipe")] Sniper,
        [Description("Shotguns/shotguns")] Shotgun,
        [Description("Explosives/expl")] Explosive,
        [Description("Handguns/handguns")] Handgun,
        [Description("IGNORE")] Kit, Perk, Misc
    }

    internal enum ItemType
    {
        Weapon = 1,
        Equipment,
        Misc
    }

    internal enum SlotType
    {
        Primary = 1,
        Secondary
    }

    internal enum ItemSlot
    {
        Primary = 1,
        Secondary,
        Equipment
    }
}