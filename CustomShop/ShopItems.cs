﻿using System.Globalization;
using Addon;
using System.Collections.Generic;
using System.Linq;

namespace CustomShop
{
    internal class ShopItems : CPlugin
    {
        private readonly List<ShopItem>                         _items;
        private readonly Dictionary<string, bool>               _clientDataSet      = new Dictionary<string, bool>();
        private readonly Dictionary<string, int>                _weaponIdToName     = new Dictionary<string, int>();
        private readonly Dictionary<string, PlayerItemData>     _clientsWeapons     = new Dictionary<string, PlayerItemData>();
        private readonly Dictionary<string, MapWeaponData>      _mapWeaponData      = new Dictionary<string, MapWeaponData>();

        // Misc variables
        private readonly int                                    _ammoPrice          = Helpers.ItemPrice("Ammo");
        private readonly Database                               _db                 = Database.GetInstance;

        /// <summary>
        /// The constructor for the "ShopItems" class
        /// </summary>
        public ShopItems()
        {
            _items = new List<ShopItem>
            {
                new ShopItem
                {
                    BoughtMessage = "^7{0} ^:bullets purchased for ^7{1} ^:points. ( ^7{2} ^:percent discount )",
                    CleanName = "Ammo",
                    Command = "ammo",
                    Price = -1,
                    Teams = new List<Teams> {Teams.Allies},
                    Category = ItemCategory.Misc
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 30,
                        AmmoClipSize = 180,
                        MaxClipSize = 250,
                        Price = _ammoPrice - 40,
                        PurchaseAmount = 54
                    },
                    Category = ItemCategory.Smg,
                    CleanName = "MP5",
                    Command = "mp5",
                    Price = Helpers.ItemPrice("Mp5"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexsmg,
                        Camo = Camos.Marine,
                    },
                    WeaponName = "iw5_mp5_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 40,
                        AmmoClipSize = 180,
                        MaxClipSize = 280,
                        Price = _ammoPrice - 50,
                        PurchaseAmount = 44
                    },
                    Category = ItemCategory.Smg,
                    CleanName = "MP7",
                    Command = "mp7",
                    Price = Helpers.ItemPrice("Mp7"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexsmg,
                        Camo = Camos.DigitalUrban,
                    },
                    WeaponName = "iw5_mp7_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 32,
                        AmmoClipSize = 180,
                        MaxClipSize = 240,
                        Price = _ammoPrice - 58,
                        PurchaseAmount = 38
                    },
                    Category = ItemCategory.Smg,
                    CleanName = "PM9",
                    Command = "pm9",
                    Price = Helpers.ItemPrice("M9"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexsmg,
                        Camo = Camos.Snake,
                    },
                    WeaponName = "iw5_m9_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 50,
                        AmmoClipSize = 180,
                        MaxClipSize = 270,
                        Price = _ammoPrice - 47,
                        PurchaseAmount = 46
                    },
                    Category = ItemCategory.Smg,
                    CleanName = "P90",
                    Command = "p90",
                    Price = Helpers.ItemPrice("P90"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexsmg,
                        Camo = Camos.Marine,
                    },
                    WeaponName = "iw5_p90_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 36,
                        AmmoClipSize = 180,
                        MaxClipSize = 300,
                        Price = _ammoPrice + 10,
                        PurchaseAmount = 60
                    },
                    Category = ItemCategory.Smg,
                    CleanName = "PP90",
                    Command = "pp90",
                    Price = Helpers.ItemPrice("Pp90M1"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexsmg,
                        Camo = Camos.Snow,
                    },
                    WeaponName = "iw5_pp90m1_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 32,
                        AmmoClipSize = 180,
                        MaxClipSize = 430,
                        Price = _ammoPrice + 15,
                        PurchaseAmount = 70
                    },
                    Category = ItemCategory.Smg,
                    CleanName = "UMP45",
                    Command = "ump45",
                    Price = Helpers.ItemPrice("Ump45"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexsmg,
                        Camo = Camos.Gold,
                    },
                    WeaponName = "iw5_ump45_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 100,
                        AmmoClipSize = 180,
                        MaxClipSize = 600,
                        Price = _ammoPrice + 47,
                        PurchaseAmount = 60
                    },
                    Category = ItemCategory.Lmg,
                    CleanName = "M60",
                    Command = "m60",
                    Price = Helpers.ItemPrice("M60"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexlmg,
                        Camo = Camos.Marine,
                    },
                    WeaponName = "iw5_m60_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 100,
                        AmmoClipSize = 180,
                        MaxClipSize = 540,
                        Price = _ammoPrice + 40,
                        PurchaseAmount = 52
                    },
                    Category = ItemCategory.Lmg,
                    CleanName = "MK46",
                    Command = "mk46",
                    Price = Helpers.ItemPrice("Mk46"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexlmg,
                        Camo = Camos.Hex,
                    },
                    WeaponName = "iw5_mk46_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 100,
                        AmmoClipSize = 180,
                        MaxClipSize = 700,
                        Price = _ammoPrice + 30,
                        PurchaseAmount = 50
                    },
                    Category = ItemCategory.Lmg,
                    CleanName = "Pech",
                    Command = "pech",
                    Price = Helpers.ItemPrice("Pech"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexlmg,
                        Camo = Camos.Autumn,
                    },
                    WeaponName = "iw5_pecheneg_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 100,
                        AmmoClipSize = 180,
                        MaxClipSize = 600,
                        Price = _ammoPrice + 47,
                        PurchaseAmount = 60
                    },
                    Category = ItemCategory.Lmg,
                    CleanName = "SA80",
                    Command = "l86",
                    Price = Helpers.ItemPrice("Sa80"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexlmg,
                        Camo = Camos.Blue,
                    },
                    WeaponName = "iw5_sa80_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 100,
                        AmmoClipSize = 180,
                        MaxClipSize = 820,
                        Price = _ammoPrice + 60,
                        PurchaseAmount = 80
                    },
                    Category = ItemCategory.Lmg,
                    CleanName = "MG36",
                    Command = "mg36",
                    Price = Helpers.ItemPrice("Mg36"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflexlmg,
                        Camo = Camos.DigitalUrban,
                    },
                    WeaponName = "iw5_mg36_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 30,
                        AmmoClipSize = 180,
                        MaxClipSize = 420,
                        Price = _ammoPrice + 13,
                        PurchaseAmount = 72
                    },
                    Category = ItemCategory.Assualt,
                    CleanName = "M4",
                    Command = "m4",
                    Price = Helpers.ItemPrice("M4"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.rof,
                        Camo = Camos.Marine,
                    },
                    WeaponName = "iw5_m4_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 30,
                        AmmoClipSize = 140,
                        MaxClipSize = 480,
                        Price = _ammoPrice + 7,
                        PurchaseAmount = 58
                    },
                    Category = ItemCategory.Assualt,
                    CleanName = "AK47",
                    Command = "ak47",
                    Price = Helpers.ItemPrice("Ak47"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflex,
                        Camo = Camos.Gold,
                        Reticle = Reticles.MilDot
                    },
                    WeaponName = "iw5_ak47_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 30,
                        AmmoClipSize = 180,
                        MaxClipSize = 380,
                        Price = _ammoPrice + 8,
                        PurchaseAmount = 50
                    },
                    Category = ItemCategory.Assualt,
                    CleanName = "M16",
                    Command = "m16",
                    Price = Helpers.ItemPrice("M16"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflex,
                        Camo = Camos.Winter,
                    },
                    WeaponName = "iw5_m16_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 40,
                        AmmoClipSize = 180,
                        MaxClipSize = 400,
                        Price = _ammoPrice + 6,
                        PurchaseAmount = 42
                    },
                    Category = ItemCategory.Assualt,
                    CleanName = "Fad",
                    Command = "fad",
                    Price = Helpers.ItemPrice("Fad"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflex,
                        Camo = Camos.Choco,
                    },
                    WeaponName = "iw5_fad_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 30,
                        AmmoClipSize = 180,
                        MaxClipSize = 322,
                        Price = _ammoPrice + 2,
                        PurchaseAmount = 42
                    },
                    Category = ItemCategory.Assualt,
                    CleanName = "Acr",
                    Command = "acr",
                    Price = Helpers.ItemPrice("Acr"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflex,
                        Camo = Camos.Red,
                    },
                    WeaponName = "iw5_acr_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 20,
                        AmmoClipSize = 180,
                        MaxClipSize = 370,
                        Price = _ammoPrice + 15,
                        PurchaseAmount = 66
                    },
                    Category = ItemCategory.Assualt,
                    CleanName = "MK14",
                    Command = "mk14",
                    Price = Helpers.ItemPrice("Mk14"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflex,
                        Camo = Camos.Hex,
                    },
                    WeaponName = "iw5_mk14_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 30,
                        AmmoClipSize = 180,
                        MaxClipSize = 500,
                        Price = _ammoPrice + 20,
                        PurchaseAmount = 64
                    },
                    Category = ItemCategory.Assualt,
                    CleanName = "Scar",
                    Command = "scar",
                    Price = Helpers.ItemPrice("Scar"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflex,
                        Camo = Camos.Snake,
                    },
                    WeaponName = "iw5_scar_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 30,
                        AmmoClipSize = 180,
                        MaxClipSize = 350,
                        Price = _ammoPrice + 10,
                        PurchaseAmount = 54
                    },
                    Category = ItemCategory.Assualt,
                    CleanName = "G36",
                    Command = "g36",
                    Price = Helpers.ItemPrice("G36C"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflex,
                        Camo = Camos.Multicam,
                    },
                    WeaponName = "iw5_g36c_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 30,
                        AmmoClipSize = 180,
                        MaxClipSize = 270,
                        Price = _ammoPrice,
                        PurchaseAmount = 46
                    },
                    Category = ItemCategory.Assualt,
                    CleanName = "CM901",
                    Command = "cm901",
                    Price = Helpers.ItemPrice("Cm901"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.reflex,
                        Camo = Camos.Gold,
                    },
                    WeaponName = "iw5_cm901_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 10,
                        AmmoClipSize = 50,
                        MaxClipSize = 120,
                        Price = _ammoPrice + 43,
                        PurchaseAmount = 20
                    },
                    Category = ItemCategory.Sniper,
                    CleanName = "Barret",
                    Command = "barret",
                    Price = Helpers.ItemPrice("Barret"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.scopevz,
                        Camo = Camos.DigitalUrban
                    },
                    WeaponName = "iw5_barrett_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 20,
                        AmmoClipSize = 40,
                        MaxClipSize = 100,
                        Price = _ammoPrice + 30,
                        PurchaseAmount = 14
                    },
                    Category = ItemCategory.Sniper,
                    CleanName = "RSASS",
                    Command = "rsass",
                    Price = Helpers.ItemPrice("Rsass"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.scopevz,
                        Camo = Camos.Snow
                    },
                    WeaponName = "iw5_rsass_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 5,
                        AmmoClipSize = 50,
                        MaxClipSize = 70,
                        Price = _ammoPrice + 12,
                        PurchaseAmount = 10
                    },
                    Category = ItemCategory.Sniper,
                    CleanName = "Msr",
                    Command = "msr",
                    Price = Helpers.ItemPrice("Msr"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.scopevz,
                        Camo = Camos.Gold
                    },
                    WeaponName = "iw5_msr_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 10,
                        AmmoClipSize = 58,
                        MaxClipSize = 420,
                        Price = _ammoPrice + 8,
                        PurchaseAmount = 20
                    },
                    Category = ItemCategory.Sniper,
                    CleanName = "Dragunov",
                    Command = "dragunov",
                    Price = Helpers.ItemPrice("Dragunov"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.scopevz
                    },
                    WeaponName = "iw5_dragunov_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 5,
                        AmmoClipSize = 70,
                        MaxClipSize = 106,
                        Price = _ammoPrice + 13,
                        PurchaseAmount = 13
                    },
                    Category = ItemCategory.Sniper,
                    CleanName = "L96A1",
                    Command = "l96a1",
                    Price = Helpers.ItemPrice("L96A1"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.scopevz
                    },
                    WeaponName = "iw5_l96a1_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 5,
                        AmmoClipSize = 60,
                        MaxClipSize = 120,
                        Price = _ammoPrice + 40,
                        PurchaseAmount = 18
                    },
                    Category = ItemCategory.Sniper,
                    CleanName = "AS50",
                    Command = "as50",
                    Price = Helpers.ItemPrice("As50"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.scopevz
                    },
                    WeaponName = "iw5_as50_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 10,
                        AmmoClipSize = 80,
                        MaxClipSize = 140,
                        Price = _ammoPrice + 30,
                        PurchaseAmount = 18
                    },
                    Category = ItemCategory.Shotgun,
                    CleanName = "1887",
                    Command = "model",
                    Price = Helpers.ItemPrice("Model"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.grip,
                        Camo = Camos.Marine
                    },
                    WeaponName = "iw5_1887_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 8,
                        AmmoClipSize = 60,
                        MaxClipSize = 128,
                        Price = _ammoPrice + 18,
                        PurchaseAmount = 22
                    },
                    Category = ItemCategory.Shotgun,
                    CleanName = "AA12",
                    Command = "aa12",
                    Price = Helpers.ItemPrice("Aa12"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.grip,
                        Camo = Camos.Marine
                    },
                    WeaponName = "iw5_aa12_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 12,
                        AmmoClipSize = 74,
                        MaxClipSize = 104,
                        Price = _ammoPrice + 16,
                        PurchaseAmount = 14
                    },
                    Category = ItemCategory.Shotgun,
                    CleanName = "USAS12",
                    Command = "usas12",
                    Price = Helpers.ItemPrice("Usas12"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.grip,
                        Camo = Camos.DigitalUrban
                    },
                    WeaponName = "iw5_usas12_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 8,
                        AmmoClipSize = 54,
                        MaxClipSize = 86,
                        Price = _ammoPrice + 4,
                        PurchaseAmount = 12
                    },
                    Category = ItemCategory.Shotgun,
                    CleanName = "SPAS12",
                    Command = "spas12",
                    Price = Helpers.ItemPrice("Spas12"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.grip
                    },
                    WeaponName = "iw5_spas12_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 12,
                        AmmoClipSize = 72,
                        MaxClipSize = 130,
                        Price = _ammoPrice + 25,
                        PurchaseAmount = 20
                    },
                    Category = ItemCategory.Shotgun,
                    CleanName = "Striker",
                    Command = "striker",
                    Price = Helpers.ItemPrice("Striker"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponExtras = new WeaponExtras
                    {
                        Att1 = Attachments.grip
                    },
                    WeaponName = "iw5_striker_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 1,
                        AmmoClipSize = 10,
                        MaxClipSize = 20,
                        Price = _ammoPrice + 60,
                        PurchaseAmount = 4
                    },
                    Category = ItemCategory.Explosive,
                    CleanName = "Rpg",
                    Command = "rpg",
                    Price = Helpers.ItemPrice("Rpg"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponName = "rpg_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 4,
                        AmmoClipSize = 50,
                        MaxClipSize = 70,
                        Price = _ammoPrice + 53,
                        PurchaseAmount = 10
                    },
                    Category = ItemCategory.Explosive,
                    CleanName = "XM25",
                    Command = "xm25",
                    Price = Helpers.ItemPrice("Xm25"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponName = "xm25_mp"
                },
                new ShopItem
                {
                    Category = ItemCategory.Explosive,
                    CleanName = "Claymore",
                    Command = "claymore",
                    EquipmentName = "claymore_mp",
                    EquipmentAmmo = 2,
                    Price = Helpers.ItemPrice("Claymore"),
                    Teams = new List<Teams> {Teams.Allies}
                },
                new ShopItem
                {
                    Category = ItemCategory.Misc,
                    CleanName = "TK",
                    Command = "tk",
                    Enabled = false,
                    EquipmentName = "throwingknife_mp",
                    PerkName = "throwingknife_mp",
                    Price = Helpers.ItemPrice("ThrowingKnife"),
                    Teams = new List<Teams> {Teams.Axis}
                },
                new ShopItem
                {
                    Advertised = true,
                    BoughtMessage = "^2Your riot shield is in your secondary slot",
                    Category = ItemCategory.Misc,
                    CleanName = "Riot Shield",
                    Command = "riot",
                    EquipmentName = "riotshield_mp",
                    EquipmentRePurchase = false,
                    Price = Helpers.ItemPrice("Shield"),
                    Teams = new List<Teams> {Teams.Axis}
                },
                new ShopItem
                {
                    Category = ItemCategory.Misc,
                    CleanName = "Faster Melee",
                    Command = "fastermelee",
                    Enabled = false,
                    Price = Helpers.ItemPrice("Fm"),
                    PerkName = "specialty_fastermelee",
                    Teams = new List<Teams> {Teams.Allies}
                },
                new ShopItem
                {
                    Category = ItemCategory.Misc,
                    CleanName = "Fast Reload",
                    Command = "fastreload",
                    Enabled = false,
                    Price = Helpers.ItemPrice("Fr"),
                    PerkName = "specialty_fastreload",
                    Teams = new List<Teams> {Teams.Allies},
                },
                new ShopItem
                {
                    Category = ItemCategory.Misc,
                    CleanName = "Scavenger",
                    Command = "scavenger",
                    Enabled = false,
                    Price = Helpers.ItemPrice("Sc"),
                    PerkName = "specialty_scavenger",
                    Teams = new List<Teams> {Teams.Allies},
                },
                new ShopItem
                {
                    Category = ItemCategory.Misc,
                    CleanName = "Blindeye",
                    Command = "blindeye",
                    Enabled = false,
                    Price = Helpers.ItemPrice("Be"),
                    PerkName = "specialty_blindeye",
                    Teams = new List<Teams> {Teams.Allies},
                },
                new ShopItem
                {
                    Category = ItemCategory.Misc,
                    CleanName = "Bullet Accuracy",
                    Command = "bulletaccuracy",
                    Enabled = false,
                    Price = Helpers.ItemPrice("Ba"),
                    PerkName = "specialty_bulletaccuracy",
                    Teams = new List<Teams> {Teams.Allies}
                },
                new ShopItem
                {
                    BoughtMessage = "^2EMP was set off, attack now",
                    Category = ItemCategory.Misc,
                    CleanName = "Emp",
                    Command = "emp",
                    Enabled = false,
                    OffHandWeaponAmmo = 2,
                    OffHandWeaponName = "emp_grenade_mp",
                    Price = Helpers.ItemPrice("Emp"),
                    Teams = new List<Teams> {Teams.Axis}
                },
                new ShopItem
                {
                    Advertised = true,
                    BoughtMessage = "^2UAV is active",
                    Category = ItemCategory.Misc,
                    CleanName = "Uav",
                    Command = "uav",
                    Price = Helpers.ItemPrice("UavCost"),
                    Teams = new List<Teams> {Teams.Axis},
                    Uav = true
                },
                new ShopItem
                {
                    BoughtMessage = "^2Juiced bought!",
                    Category = ItemCategory.Misc,
                    CleanName = "Juiced",
                    Command = "juiced",
                    Enabled = false,
                    Price = Helpers.ItemPrice("JuicedCost"),
                    PerkName = "specialty_juiced",
                    Teams = new List<Teams> {Teams.Axis}
                },
                new ShopItem
                {
                    Category = ItemCategory.Misc,
                    CleanName = "Cold Blooded",
                    Command = "coldblood",
                    Enabled = false,
                    Price = Helpers.ItemPrice("Cb"),
                    PerkName = "specialty_coldblooded",
                    Teams = new List<Teams> {Teams.Allies, Teams.Axis}
                },
                new ShopItem
                {
                    Category = ItemCategory.Misc,
                    CleanName = "Stalker",
                    Command = "stalker",
                    Enabled = false,
                    Price = Helpers.ItemPrice("Striker"),
                    PerkName = "specialty_stalker",
                    Teams = new List<Teams> {Teams.Allies, Teams.Axis}
                },
                new ShopItem
                {
                    Advertised = true,
                    Category = ItemCategory.Misc,
                    CleanName = "Health +50",
                    Command = "health",
                    Health = 50,
                    Price = Helpers.ItemPrice("Health"),
                    Teams = new List<Teams> {Teams.Axis}
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 6,
                        AmmoClipSize = 30,
                        MaxClipSize = 58,
                        Price = _ammoPrice - 5,
                        PurchaseAmount = 20
                    },
                    Category = ItemCategory.Handgun,
                    CleanName = "44 Magnum",
                    Command = "magnum",
                    Price = Helpers.ItemPrice("Magnum44"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponName = "iw5_44magnum_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 12,
                        AmmoClipSize = 50,
                        MaxClipSize = 70,
                        Price = _ammoPrice - 9,
                        PurchaseAmount = 15
                    },
                    Category = ItemCategory.Handgun,
                    CleanName = "USP45",
                    Command = "usp45",
                    Price = Helpers.ItemPrice("Usp45"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponName = "iw5_usp45_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 8,
                        AmmoClipSize = 46,
                        MaxClipSize = 60,
                        Price = _ammoPrice - 10,
                        PurchaseAmount = 12
                    },
                    Category = ItemCategory.Handgun,
                    CleanName = "Desert Eagle",
                    Command = "deserteagle",
                    Price = Helpers.ItemPrice("DesertEagle"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponName = "iw5_deserteagle_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 6,
                        AmmoClipSize = 30,
                        MaxClipSize = 68,
                        Price = _ammoPrice - 3,
                        PurchaseAmount = 16
                    },
                    Category = ItemCategory.Handgun,
                    CleanName = "MP412",
                    Command = "mp412",
                    Price = Helpers.ItemPrice("Mp412"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponName = "iw5_mp412_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 12,
                        AmmoClipSize = 50,
                        MaxClipSize = 72,
                        Price = _ammoPrice - 12,
                        PurchaseAmount = 15
                    },
                    Category = ItemCategory.Handgun,
                    CleanName = "P99",
                    Command = "p99",
                    Price = Helpers.ItemPrice("P99"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponName = "iw5_p99_mp"
                },
                new ShopItem
                {
                    Ammo = new Ammo
                    {
                        AmmoAmount = 16,
                        AmmoClipSize = 40,
                        MaxClipSize = 80,
                        Price = _ammoPrice - 8,
                        PurchaseAmount = 22
                    },
                    Category = ItemCategory.Handgun,
                    CleanName = "Five Seven",
                    Command = "fnfive",
                    Price = Helpers.ItemPrice("FiveSeven"),
                    Teams = new List<Teams> {Teams.Allies},
                    WeaponName = "iw5_fnfiveseven_mp"
                },
                new ShopItem
                {
                    Advertised = true,
                    BoughtMessage = "^6Juggernaut is active",
                    Category = ItemCategory.Misc,
                    CleanName = "Juggernaut",
                    Command = "jugg",
                    Health = 600,
                    MaxHealth = true,
                    PlayerModel = "mp_fullbody_ally_juggernaut",
                    Price = Helpers.ItemPrice("Juggernaut"),
                    SpeedScale = 1.1f,
                    Teams = new List<Teams> {Teams.Axis}
                }
            };
        }

        /// <summary>
        /// Runs through all the shop equipment and weapons and collects the weapon id associated with them
        /// </summary>
        public void SetWeaponIds()
        {
            _weaponIdToName.Clear();

            foreach (var item in _items.Where(x => x.EquipmentName != null || x.WeaponName != null))
            {
                var weaponName = item.EquipmentName ?? item.WeaponName;

                _weaponIdToName.Add(weaponName,
                                    item.WeaponExtras != null
                                        ? GetWeapon(weaponName, item.WeaponExtras.Att1, item.WeaponExtras.Att2,
                                                    item.WeaponExtras.Att3, item.WeaponExtras.Camo,
                                                    item.WeaponExtras.Reticle)
                                        : GetWeapon(weaponName));
            }
        }

        /// <summary>
        /// If the current map hasn't been added yet a new entry will be created so if a player
        /// want's to reset their weapon(s) they can using these defaults
        /// </summary>
        /// <param name="weaponData"></param>
        public void SetMapWeapons(MapWeaponData weaponData)
        {
            _mapWeaponData.Add(GetDvar("mapname"), weaponData);
        }

        /// <summary>
        /// Determines if the current map weapon data has been set
        /// </summary>
        /// <returns></returns>
        public bool MapWeaonDataExists()
        {
            return _mapWeaponData.ContainsKey(GetDvar("mapname"));
        }

        /// <summary>
        /// Resets the players current weapon/all their weapons with the map defaults
        /// </summary>
        /// <param name="client"></param>
        /// <param name="allWeapons"></param>
        public void ResetPlayerWeapons(ServerClient client, bool allWeapons)
        {
            string message = null;

            var md = _mapWeaponData[GetDvar("mapname")];
            var pl = _clientsWeapons[client.XUID].Human;

            if (allWeapons)
            {
                var primaryReset = ResetPlayerWeaponSlot(client, md, SlotType.Primary);
                var secondaryReset = ResetPlayerWeaponSlot(client, md, SlotType.Secondary);

                if (!primaryReset && secondaryReset)
                {
                    message = "^:Your secondary weapon has been reset";
                }
                else if (primaryReset && !secondaryReset)
                {
                    message = "^:Your primary weapon has been reset";
                }
                else if (!primaryReset)
                {
                    message = "^:Your primary and secondary weapons are already defaulted";
                }
                else
                {
                    message = "^3Your primary and secondary weapons have been reset";
                }

                // Update the players weapon data
                pl.PrimaryWeaponName = null;
                pl.SecondaryWeaponName = null;

                // Update the players current weapon
                client.Other.CurrentWeapon = client.Other.PrimaryWeapon;
                client.Other.CurrentWeaponAkimbo = false;

                TellClient(client.ClientNum, string.Format("^2[SHOP] {0}", message), true);
            }
            else
            {
                var weaponId = -1;
                var weaponSlot = client.Other.CurrentWeapon == client.Other.PrimaryWeapon ? SlotType.Primary : SlotType.Secondary;

                switch (weaponSlot)
                {
                    case SlotType.Primary:
                        if (ResetPlayerWeaponSlot(client, md, SlotType.Primary))
                        {
                            weaponId = client.Other.PrimaryWeapon;
                            pl.PrimaryWeaponName = null;
                        }
                        else
                        {
                            message = "^:Your primary weapon is already defaulted";
                        }
                        break;

                    case SlotType.Secondary:
                        if (ResetPlayerWeaponSlot(client, md, SlotType.Secondary))
                        {
                            weaponId = client.Other.SecondaryWeapon;
                            pl.SecondaryWeaponName = null;
                        }
                        else
                        {
                            message = "^:Your secondary weapon is already defaulted";
                        }
                        break;
                }

                if (weaponId != -1)
                {
                    client.Other.CurrentWeapon = weaponId;
                    client.Other.CurrentWeaponAkimbo = false;
                }

                TellClient(client.ClientNum,
                           string.Format("^2[SHOP] {0}",
                                         message ??
                                         string.Format("^:Your ^7{0} ^:weapon has been reset",
                                                       weaponSlot.ToString().ToLower())), true);
            }
        }

        /// <summary>
        /// Retrieves all enabled shop items and returns a list of the commands for them
        /// </summary>
        /// <returns></returns>
        public string GetItemCommands()
        {
            return string.Join(" ", _items.Where(x => x.Enabled).Select(x => x.Command).ToArray());
        }

        /// <summary>
        /// Attempts to find the shop item that was specified
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public ShopItem DetermineShopItem(string command)
        {
            return _items.FirstOrDefault(x => x.Command == command && x.Enabled);
        }

        /// <summary>
        /// Returns the "ShopItem" object for the given item name
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public ShopItem GetItemInfo(string item)
        {
            return _items.FirstOrDefault(x => x.Command == item);
        }

        /// <summary>
        /// Finds all the enabled items with the given item type
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public List<ShopItem> GetItemsByType(ItemCategory category)
        {
            return _items.Where(x => x.Enabled && x.Category == category).ToList();
        }

        /// <summary>
        /// Finds all the shop items by the teams specified
        /// </summary>
        /// <param name="teams"></param>
        /// <returns></returns>
        public List<ShopItem> GetItemsByTeam(List<Teams> teams)
        {
            return
                (from item in _items.Where(x => x.Enabled) from team in teams where item.Teams.Contains(team) select item)
                    .ToList();
        }

        /// <summary>
        /// Retrieves a list of advetised items for the given team
        /// </summary>
        /// <param name="team"></param>
        /// <returns></returns>
        public List<ShopItem> GetAdvertisedItems(Teams team)
        {
            return _items.Where(x => x.Advertised && x.Enabled && x.Teams.Contains(team)).ToList();
        } 

        /// <summary>
        /// Retrieves the shop item for the given weapon name
        /// </summary>
        /// <param name="weaponName"></param>
        /// <returns></returns>
        public ShopItem GetItemByWeaponName(string weaponName)
        {
            return _items.FirstOrDefault(x => x.WeaponName == weaponName);
        }

        /// <summary>
        /// Retrieves the shop item for the given equipment name
        /// </summary>
        /// <param name="equipmentName"></param>
        /// <returns></returns>
        public ShopItem GetItemByEquipmentName(string equipmentName)
        {
            return _items.FirstOrDefault(x => x.EquipmentName == equipmentName);
        }

        /// <summary>
        /// Retrieves the shop item using the given weapon id
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public ShopItem GetItemByNameUsingCurrentWeapon(ServerClient client)
        {
            var weaponCache = _weaponIdToName.FirstOrDefault(x => x.Value == client.Other.CurrentWeapon);
            return weaponCache.Key != null ? GetItemByWeaponName(weaponCache.Key) : null;
        }

        /// <summary>
        /// Returns a list of shop item as info items which can be used for different things
        /// </summary>
        /// <param name="teams"></param>
        /// <returns></returns>
        public List<ItemInfo> GetItemsAsInfoList(List<Teams> teams = null)
        {
            if (teams == null)
            {
                teams = new List<Teams> {Teams.Allies};
            }

            // Get all the shop items for the specified teams, if no teams where given then
            // humans will be set as the default team to search for
            return GetItemsByTeam(teams).Select(x => new ItemInfo(x.Command, x.CleanName, x.Price)).ToList();
        }

        /// <summary>
        /// Returns a string with the specified items formatted in a readable list
        /// </summary>
        /// <param name="items"></param>
        /// <param name="seperator"></param>
        /// <returns></returns>
        public string GetItemsFormatted(IList<ShopItem> items, string seperator = "^7|^3")
        {
            var list = new List<string>();

            if (items.Count > 0)
            {
                list.AddRange(items.Select(item => string.Format("/{0}^2[{1}]", item.Command, item.Price)));
            }

            return "^3" + string.Join(seperator, list.ToArray());
        }

        /// <summary>
        /// Returns a string with the specified items formatted in a readable list
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public string GetItemsFormatted(string command)
        {
            return GetItemsFormatted(GetItemsByType(GetItemTypeFromCommand(command)));
        }

        /// <summary>
        /// Returns a string with the shop commands, these commands only relate to allies aka humans
        /// </summary>
        /// <returns></returns>
        public string GetShopCommands()
        {
            var commands = new List<string>();
            var enums = Helpers.EnumToList<ItemCategory>();

            if (enums != null)
            {
                foreach (var val in enums)
                {
                    var desciption = Helpers.GetEnumDescription(val);

                    if (val.ToString() == desciption || desciption.Contains("IGNORE")) continue;

                    var split = desciption.Split('/');
                    commands.Add(string.Format("/{1}^7-^2{0}", split[0], split[1]));
                }
            }

            return "^3" + string.Join("^7|^3", commands.ToArray());
        }

        /// <summary>
        /// Creates a new weapon data entry for the client
        /// </summary>
        /// <param name="client"></param>
        public void CreateWeaponDataEntry(ServerClient client)
        {
            if (_clientsWeapons.ContainsKey(client.XUID)) return;
            _clientsWeapons.Add(client.XUID, new PlayerItemData());
        }

        /// <summary>
        /// Updates the clients weapon data locally
        /// </summary>
        /// <param name="client"></param>
        public void UpdatePlayerWeaponsFromDb(ServerClient client)
        {
            if (_clientDataSet.ContainsKey(client.XUID)) return;
            string primaryWeapon = null, secondaryWeapon = null, equipment = null;

            // Get the clients item data from the database if it exists
            var table = client.Team == Teams.Allies ? "ItemsToHumans" : "ItemsToInfected";
            var playerId = _db.GetPlayerData<int>("id", client.XUID);

            if (_db.GetPlayerData<int>("count(*)", playerId.ToString(CultureInfo.InvariantCulture), table, true) == 1)
            {
                var fields = new[] { "primary_weapon", "secondary_weapon" };

                if (client.Team == Teams.Axis)
                {
                    fields[fields.Length] = "equipment";
                }

                var result = _db.GetPlayerData(fields, playerId, table);
                var data = result.Rows.Count > 0 ? result.Rows[0] : null;

                // Set the item data
                if (data != null)
                {
                    var primaryWeaponValue = data["primary_weapon"].ToString();
                    var secondaryWeaponValue = data["secondary_weapon"].ToString();

                    primaryWeapon = string.IsNullOrEmpty(primaryWeaponValue) ? null : primaryWeaponValue;
                    secondaryWeapon = string.IsNullOrEmpty(secondaryWeaponValue) ? null : secondaryWeaponValue;

                    if (client.Team == Teams.Axis)
                    {
                        var equipmentValue = data["equipment"].ToString();
                        equipment = string.IsNullOrEmpty(equipmentValue) ? null : equipmentValue;
                    }
                }
            }

            // Update the team specific data
            var prop = client.Team == Teams.Allies ? "Human" : "Infected";
            var cData = _clientsWeapons[client.XUID];
            var iData = (WeaponData) typeof (PlayerItemData).GetProperty(prop).GetValue(cData, null);

            typeof (WeaponData).GetProperty("PrimaryWeaponName").SetValue(iData, primaryWeapon, null);
            typeof (WeaponData).GetProperty("SecondaryWeaponName").SetValue(iData, secondaryWeapon, null);
            typeof (WeaponData).GetProperty("EquipmentName").SetValue(iData, equipment, null);

            // The clients weapon data has being retrieved, no need to do it again
            _clientDataSet.Add(client.XUID, true);
        }

        /// <summary>
        /// Sets the weapons that were saved before the player was killed in action
        /// </summary>
        /// <param name="client"></param>
        public void SetPlayerWeapons(ServerClient client)
        {
            if (!_clientsWeapons.ContainsKey(client.XUID)) return;

            var data = _clientsWeapons[client.XUID].Human;

            // Set the primary and secondary weapons for the player
            SetCustomWeaponForSlot(client, data.PrimaryWeaponName, SlotType.Primary);
            SetCustomWeaponForSlot(client, data.SecondaryWeaponName, SlotType.Secondary);
        }

        /// <summary>
        /// Updates the specified weapon slot for the given player, also updates the ammo amount and
        /// the size of the clip
        /// </summary>
        /// <param name="client"></param>
        /// <param name="weaponName"></param>
        /// <param name="slotType"></param>
        private void SetCustomWeaponForSlot(ServerClient client, string weaponName, SlotType slotType)
        {
            if (weaponName == null) return;

            var item = GetItemByWeaponName(weaponName);
            var weaponId = _weaponIdToName.FirstOrDefault(x => x.Key == weaponName).Value;

            // Update the players weapon slot data
            UpdateWeaponSlot(client, slotType, weaponId, item.Ammo.AmmoAmount, item.Ammo.AmmoClipSize);

            // Finally check if the weapon was actually set, if it wasn't set a timer that runs after a 50
            // millisecond delay
            var tClient = client;
            Timing.AfterDelay(50, client, () =>
                {
                    if ((int) typeof (Other_t).GetProperty(slotType + "Weapon").GetValue(tClient.Other, null) != weaponId)
                    {
                        SetCustomWeaponForSlot(tClient, weaponName, slotType);
                    }
                    else
                    {
                        tClient.Other.CurrentWeapon = weaponId;
                        tClient.Other.CurrentWeaponAkimbo = false;

                        typeof (Other_t).GetProperty(slotType + "Weapon").SetValue(tClient.Other, weaponId, null);
                        typeof (Other_t).GetProperty(slotType + "WeaponAkimbo").SetValue(tClient.Other, false, null);
                    }
                });
        }

        /// <summary>
        /// Updates the players weapon slot with the given data
        /// </summary>
        /// <param name="client"></param>
        /// <param name="slotType"></param>
        /// <param name="weaponId"></param>
        /// <param name="ammo"></param>
        /// <param name="ammoClip"></param>
        private static void UpdateWeaponSlot(ServerClient client, SlotType slotType, int weaponId, int ammo,
                                             int ammoClip)
        {
            // Update the players weapon slot
            typeof (Other_t).GetProperty(slotType + "Weapon").SetValue(client.Other, weaponId, null);
            typeof (Other_t).GetProperty(slotType + "WeaponAkimbo").SetValue(client.Other, false, null);

            // Update the players weapon ammo amount and clip size
            typeof (Ammo_t).GetProperty(slotType + "Ammo").SetValue(client.Ammo, ammo, null);
            typeof (Ammo_t).GetProperty(slotType + "AmmoClip").SetValue(client.Ammo, ammoClip, null);
        }

        /// <summary>
        /// Checks if the player has a custom weapon set and resets it with the map default
        /// </summary>
        /// <param name="client"></param>
        /// <param name="data"></param>
        /// <param name="slotType"></param>
        /// <returns></returns>
        private bool ResetPlayerWeaponSlot(ServerClient client, MapWeaponData data, SlotType slotType)
        {
            var status = false;
            ItemSlot slot = 0;

            switch (slotType)
            {
                case SlotType.Primary:
                    status = ResetPlayerWeaponSlot(client, SlotType.Primary, data.PrimaryWeapon, data.PrimaryAmmo,
                                                   data.PrimaryAmmoClip);

                    slot = ItemSlot.Primary;
                    break;

                case SlotType.Secondary:
                    status = ResetPlayerWeaponSlot(client, SlotType.Secondary, data.SecondaryWeapon, data.SecondaryAmmo,
                                                   data.SecondaryAmmoClip);

                    slot = ItemSlot.Secondary;
                    break;
            }

            if (status)
            {
                _db.UpdatePlayeritemData(client.XUID, client.Team, slot);
            }

            return status;
        }

        /// <summary>
        /// Checks if the player has a custom weapon set and resets it with the map default
        /// </summary>
        /// <param name="client"></param>
        /// <param name="slotType"></param>
        /// <param name="weaponId"></param>
        /// <param name="ammo"></param>
        /// <param name="ammoClip"></param>
        private static bool ResetPlayerWeaponSlot(ServerClient client, SlotType slotType, int weaponId, int ammo,
                                                  int ammoClip)
        {
            // Get the players current weapon id for the given slot
            var weaponSlotId = (int) typeof (Other_t).GetProperty(slotType + "Weapon").GetValue(client.Other, null);

            if (weaponSlotId == weaponId) return false;
            UpdateWeaponSlot(client, slotType, weaponId, ammo, ammoClip);
            return true;
        }

        /// <summary>
        /// Determines if the player had a riot shield and set it as their primary weapon if they did
        /// </summary>
        /// <param name="client"></param>
        public void GiveInfectedRiotShield(ServerClient client)
        {
            if (!_clientsWeapons[client.XUID].Infected.HadRiotShield) return;
            var riotShield = GetItemByEquipmentName("riotshield_mp");

            if (riotShield == null) return;

            var weaponId = GetWeapon(riotShield.EquipmentName);
            client.Other.Equipment = weaponId;

            // Finally check if the equipment was actually set, if it wasn't set a timer that runs after a
            // 50 millisecond delay
            Timing.AfterDelay(50, client, () =>
                {
                    if (client.Other.Equipment != weaponId)
                    {
                        GiveInfectedRiotShield(client);
                    }
                });
        }

        /// <summary>
        /// Resets all the infected player data so they don't get a riot shield on the next map
        /// </summary>
        /// <param name="clients"></param>
        public void ResetRiotShields(IEnumerable<ServerClient> clients = null)
        {
            foreach (var client in (clients ?? GetClients()).Where(x => !TestClients.IsBot(x)))
            {
                var data = _clientsWeapons[client.XUID].Infected;

                // Update the players infected data
                UpdatePlayerWeaponData("PrimaryWeaponName", string.Empty, client, ref data);
                UpdatePlayerWeaponData("SecondaryWeaponName", string.Empty, client, ref data);
                UpdatePlayerWeaponData("HadRiotShield", false, client, ref data);
            }
        }

        /// <summary>
        /// Determines if a weapon has been passed through and updates the players primary and current
        /// weapon using the ID returned by the server
        /// </summary>
        /// <param name="item"></param>
        /// <param name="client"></param>
        /// <param name="command"></param>
        public string GetWeaponByNameAndSetPrimaryWeapon(ShopItem item, ref ServerClient client, string command)
        {
            if (item.WeaponName == null) return null;

            // First check if the player has bought this weapon already, if they have stop here
            var data =
                typeof (PlayerItemData).GetProperty(client.Team == Teams.Allies ? "Human" : "Infected")
                                       .GetValue(_clientsWeapons[client.XUID], null) as WeaponData;

            if (data == null)
            {
                return "^1An error occurred, please try again";
            }

            // Ensure the player hasn't already purchased this weapon
            if (data.PrimaryWeaponName == item.WeaponName || data.SecondaryWeaponName == item.WeaponName)
            {
                return "^3You've already purchased this weapon";
            }

            // Get the weapon id for the selected weapon
            var weaponId = _weaponIdToName.FirstOrDefault(x => x.Key == item.WeaponName).Value;

            // Update the players weapon using their current weapon id, this prevents an unwanted
            // weapon from being overwritten
            if ((item.SlotType > 0 && item.SlotType == SlotType.Primary) ||
                client.Other.CurrentWeapon == client.Other.PrimaryWeapon)
            {
                client.Other.PrimaryWeapon = weaponId;
                UpdatePlayerWeaponData("PrimaryWeaponName", item.WeaponName, client, ref data);
            }
            else if ((item.SlotType > 0 && item.SlotType == SlotType.Secondary) ||
                     client.Other.CurrentWeapon == client.Other.SecondaryWeapon)
            {
                client.Other.SecondaryWeapon = weaponId;
                UpdatePlayerWeaponData("SecondaryWeaponName", item.WeaponName, client, ref data);
            }

            // Update the players active weapon and set the current weapon
            client.Other.CurrentWeapon = weaponId;
            client.Other.CurrentWeaponAkimbo = false;

            return null;
        }

        /// <summary>
        /// Determines if a weapon has been passed through and updates the players equipment using the
        /// ID returned by the server
        /// </summary>
        /// <param name="item"></param>
        /// <param name="client"></param>
        /// <param name="command"></param>
        public string GetWeaponByNameAndSetEquipment(ShopItem item, ref ServerClient client, string command)
        {
            if (item.EquipmentName == null) return null;

            // First check if the player has bought this weapon already, if they have stop here
            var data =
                typeof (PlayerItemData).GetProperty(client.Team == Teams.Allies ? "Human" : "Infected")
                                       .GetValue(_clientsWeapons[client.XUID], null) as WeaponData;

            if (data == null)
            {
                return "^1An error occurred, please try again";
            }

            // Get the weapon id from the equipment name
            var weaponId = _weaponIdToName.FirstOrDefault(x => x.Key == item.EquipmentName).Value;

            // Does the player already have this?
            if ((client.Other.Equipment == weaponId || client.Other.PrimaryWeapon == weaponId ||
                client.Other.SecondaryWeapon == weaponId) && !item.EquipmentRePurchase)
            {
                var name = item.EquipmentName;

                if (data.PrimaryWeaponName == name || data.SecondaryWeaponName == name || data.EquipmentName == name)
                {
                    return "^3You've already purchased this equipment";
                }

                return "^3You already have this equipment set";
            }

            // Update the players equipment id
            client.Other.Equipment = weaponId;
            UpdatePlayerWeaponData("EquipmentName", item.EquipmentName, client, ref data);
            
            // Does the equipment need ammo?
            if (item.EquipmentAmmo > 0)
            {
                client.Ammo.EquipmentAmmo = item.EquipmentAmmo;
            }

            // Was a riot shield purchased?
            if (item.Command == "riot" && client.Team == Teams.Axis)
            {
                UpdatePlayerWeaponData("HadRiotShield", true, client, ref data);
            }

            return null;
        }

        /// <summary>
        /// Sets the primary and secondary clips for the players weapons
        /// </summary>
        /// <param name="item"></param>
        /// <param name="client"></param>
        /// <param name="command"></param>
        public string SetPrimaryAndSecondaryAmmoAndClips(ShopItem item, ref ServerClient client, string command)
        {
            if (item.WeaponName == null && command != "ammo") return null;

            // Get the players weapon data
            var isPrimaryWeapon = client.Other.CurrentWeapon == client.Other.PrimaryWeapon;
            var playerAmmo = typeof (Ammo_t).GetProperty(isPrimaryWeapon ? "PrimaryAmmo" : "SecondaryAmmo");
            var playerAmmoClip = typeof (Ammo_t).GetProperty(isPrimaryWeapon ? "PrimaryAmmoClip" : "SecondaryAmmoClip");

            var ammoAmount = (int) playerAmmo.GetValue(client.Ammo, null);
            var ammoClipAmount = (int) playerAmmoClip.GetValue(client.Ammo, null);

            // Is the player purchasing ammo or the weapon?
            switch (command)
            {
                case "ammo":
                    var weapon = GetItemByNameUsingCurrentWeapon(client);

                    // Get the ammo data for the weaon
                    var ammo = weapon.Ammo;

                    // Does the total amount of clip ammo exceed the max allowed?
                    if (ammoClipAmount == ammo.MaxClipSize)
                    {
                        return string.Format("Ammo limit reached for the ^7{0}", weapon.CleanName.ToUpper());
                    }

                    // Set the ammo data
                    if ((ammoClipAmount + ammo.PurchaseAmount) >= ammo.MaxClipSize)
                    {
                        ammoClipAmount = ammoClipAmount + ammo.MaxClipSize - ammoClipAmount;
                    }
                    else
                    {
                        ammoClipAmount += ammo.PurchaseAmount;
                    }

                    break;

                default:
                    ammoAmount = item.Ammo.AmmoAmount;
                    ammoClipAmount = item.Ammo.AmmoClipSize;
                    break;
            }

            // Update the players ammo data
            playerAmmo.SetValue(client.Ammo, ammoAmount, null);
            playerAmmoClip.SetValue(client.Ammo, ammoClipAmount, null);

            return null;
        }

        /// <summary>
        /// Determines if a weapon has been passed through and updates the players off hand weapon
        /// using the ID returned from the server
        /// </summary>
        /// <param name="item"></param>
        /// <param name="client"></param>
        /// <param name="command"></param>
        public string GetWeaponByNameAndSetOffHand(ShopItem item, ref ServerClient client, string command)
        {
            if (item.OffHandWeaponName == null) return null;

            client.Other.OffhandWeapon = GetWeapon(item.OffHandWeaponName);
            client.Ammo.OffhandAmmo = item.OffHandWeaponAmmo;

            return null;
        }

        /// <summary>
        /// Sets the players model
        /// </summary>
        /// <param name="item"></param>
        /// <param name="client"></param>
        /// <param name="command"></param>
        /// <returns></returns>
        public string SetPlayerModel(ShopItem item, ref ServerClient client, string command)
        {
            var data = _clientsWeapons[client.XUID];
            var team = client.Team == Teams.Allies ? "Human" : "Infected";
            var player = (WeaponData) typeof (PlayerItemData).GetProperty(team).GetValue(data, null);
            var model = (string) typeof (WeaponData).GetProperty("PlayerModel").GetValue(player, null);

            if (item.PlayerModel == null) return null;

            // Does the player already have this model set?
            if (item.PlayerModel == model)
            {
                return "You already have that player model set";
            }

            client.Other.SetPlayerModel(item.PlayerModel);
            client.Other.SpeedScale = item.SpeedScale;

            return null;
        }

        /// <summary>
        /// Sets the special player items
        /// </summary>
        /// <param name="item"></param>
        /// <param name="client"></param>
        /// <param name="command"></param>
        public string SetPlayerOtherSpecials(ShopItem item, ref ServerClient client, string command)
        {
            client.Other.UAV.Enabled = item.Uav;

            // Give health to the player only if they have less than 350HP
            if (item.Health > 0)
            {
                var health = client.Other.Health;

                // Make sure the players health is less than the total amount of health they're allowed to have
                // at any given time
                if (item.MaxHealth || health < 350)
                {
                    client.Other.Health = item.MaxHealth ? item.Health : (health + item.Health);
                }
                else
                {
                    return "You have reached your health limit of 350";
                }
            }

            return null;
        }

        /// <summary>
        /// Sets the players perk
        /// </summary>
        /// <param name="item"></param>
        /// <param name="client"></param>
        /// <param name="command"></param>
        public string SetPlayerPerk(ShopItem item, ref ServerClient client, string command)
        {
            if (item.PerkName == null) return null;
            client.Other.SetPerk(GetPerk(item.PerkName));
            return null;
        }

        /// <summary>
        /// Retrieves the item type for the given command name
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private static ItemCategory GetItemTypeFromCommand(string command)
        {
            ItemCategory category = 0;

            switch (command)
            {
                case "smg":
                    category = ItemCategory.Smg;
                    break;

                case "lmg":
                    category = ItemCategory.Lmg;
                    break;

                case "assault":
                    category = ItemCategory.Assualt;
                    break;

                case "snipe":
                    category = ItemCategory.Sniper;
                    break;

                case "shotguns":
                    category = ItemCategory.Shotgun;
                    break;

                case "expl":
                    category = ItemCategory.Explosive;
                    break;

                case "handgun":
                    category = ItemCategory.Handgun;
                    break;
            }

            return category;
        }

        /// <summary>
        /// Updates the players weapon data so the weapon can be restored later on
        /// </summary>
        /// <param name="prop"></param>
        /// <param name="value"></param>
        /// <param name="client"></param>
        /// <param name="data"></param>
        private void UpdatePlayerWeaponData(string prop, object value, ServerClient client, ref WeaponData data)
        {
            // Update the property value
            typeof (WeaponData).GetProperty(prop).SetValue(data, value, null);

            // Determine which slot we're updating
            ItemSlot itemSlot = 0;

            switch (prop)
            {
                case "EquipmentName":
                    itemSlot = ItemSlot.Equipment;
                    break;

                case "PrimaryWeaponName":
                    itemSlot = ItemSlot.Primary;
                    break;

                case "SecondaryWeaponName":
                    itemSlot = ItemSlot.Secondary;
                    break;

                case "HadRiotShield":
                    itemSlot = ItemSlot.Equipment;
                    value = string.Empty;
                    break;
            }

            if (itemSlot > 0)
            {
                _db.UpdatePlayeritemData(client.XUID, client.Team, itemSlot, value.ToString());
            }
        }
    }

    /// <summary>
    /// Simply holds the data for shop items
    /// </summary>
    internal class ItemInfo
    {
        public string Command, Name;
        public int Points;

        public ItemInfo(string command, string name, int points)
        {
            Command = command;
            Name = name;
            Points = points;
        }
    }

    /// <summary>
    /// Simply holds data about a message
    /// </summary>
    internal class ShopDetails
    {
        public string AlliesMessage, AxisMessage;
    }

    /// <summary>
    /// Holds all the information about the shop item
    /// </summary>
    internal class ShopItem
    {
        public Ammo             Ammo                    = null;
        public ItemCategory     Category;

        public string           BoughtMessage,
                                CleanName,
                                Command,
                                EquipmentName           = null,
                                OffHandWeaponName,
                                PerkName,
                                PlayerModel             = null,
                                WeaponName              = null;

        public bool             Advertised              = false,
                                Enabled                 = true,
                                EquipmentRePurchase     = true,
                                MaxHealth               = false;

        public int              EquipmentAmmo,
                                Health,
                                OffHandWeaponAmmo,
                                Price;

        public float            SpeedScale;

        public List<Teams>      Teams;
        public bool             Uav;

        public WeaponExtras     WeaponExtras            = null;
        public SlotType         SlotType                = 0;
    }

    /// <summary>
    /// Holds information about how much ammo costs for the weapon and how much ammo the weapon
    /// is allowed to hold before been denied
    /// </summary>
    internal class Ammo
    {
        public int  AmmoAmount,
                    AmmoClipSize,
                    MaxClipSize         = -1,
                    Price,
                    PurchaseAmount;
    }

    /// <summary>
    /// Holds extra information about the weapon such as attachments, camo etc...
    /// </summary>
    internal class WeaponExtras
    {
        public Attachments      Att1        = Attachments.none;
        public Attachments      Att2        = Attachments.none;
        public Attachments      Att3        = Attachments.none;
        public Camos            Camo        = Camos.None;
        public Reticles         Reticle     = Reticles.None;
    }

    /// <summary>
    /// Holds all the item info for the help huds
    /// </summary>
    internal class TeamItems
    {
        public List<ItemInfo> Allies, Axis;
    }

    /// <summary>
    /// Holds data about what weapon the client had before they died, this is currently not
    /// stored forever so it only last's as long as the server is online
    /// </summary>
    internal class PlayerItemData
    {
        private WeaponData _human       = new WeaponData();
        private WeaponData _infected    = new WeaponData();

        public WeaponData Human
        {
            get { return _human; }
            set { _human = value; }
        }

        public WeaponData Infected
        {
            get { return _infected; }
            set { _infected = value; }
        }
    }

    internal class WeaponData
    {
        public string   EquipmentName           { get; set; }
        public bool     HadRiotShield           { get; set; }
        public string   PrimaryWeaponName       { get; set; }
        public string   SecondaryWeaponName     { get; set; }
        public string   PlayerModel             { get; set; }

        public WeaponData()
        {
            EquipmentName           = null;
            PrimaryWeaponName       = null;
            SecondaryWeaponName     = null;
            PlayerModel             = null;
        }
    }

    /// <summary>
    /// A simple class to hold map weapon data
    /// </summary>
    internal class MapWeaponData
    {
        public int  PrimaryAmmo,
                    PrimaryAmmoClip,
                    PrimaryWeapon,
                    SecondaryAmmo,
                    SecondaryAmmoClip,
                    SecondaryWeapon;
    }
}